# Introduction  
Ce projet est issu de l'unit� 3PJT. Pour la promotion 2018 - 2019 Supinfo �crit par CHERVY Brian / SAD

Les membres qui ont travaill�s sur ce projet sont: 
	
- 298261 Bruno LARIBIERE

# Descritpion

SupBankChain est un syst�me de banque d�centralis�e bas� sur un r�seau P2P mais aussi un syst�me financier proche du bitcoin

# Technologies

**Multiplateforme**
- SupBankChain.Core ==> .NET Core 2.1 (librairie)
- SupBankChain.App ==> .NET Core 2.1
- SupBankChain.Nodes ==> ASP.NET Core 2.0

![alt text](http://image.noelshack.com/fichiers/2018/47/2/1542750116-architectureglobale.jpg)


# P2P network
R�seau decentralis� permettant la communication directe entre les nodes.

![alt text](http://image.noelshack.com/fichiers/2018/47/2/1542745892-p2ppeerjoinsystem.jpg)

# Nodes
Sont l'ensemble des utilisateurs connect�s au r�seau participant au maintient de ce dernier
> - Font partie du r�seau P2P
> - Travaillent � r�soudre la preuve de travail
> - R�solvent les transactions
> - Gagnent une r�compense monn�taire � chaque validation de block

![alt text](http://image.noelshack.com/fichiers/2018/47/2/1542745892-p2pglobalsystem.png)


# BlockChain
D�signe l'ensemble de la chaine de block. Cette chaine s�curise l'ensemble des data inscrites. **Toute �criture est d�finitive, seul l'ajout de nouveaux blocks est possible.**

# Block
Contient des datas comme des transactions financi�res. 
> - Il poss�de la r�f�rence du block pr�c�dent. Ce syst�me g�n�re la blockchain
> - Il poss�de le hash de ses datas
> - Il poss�de le hash de lui m�me
> - Il poss�de une preuve de travail


# Installation
Multiplateforme
> Windows
> 
> Mac
> 
> Linux

# En cas de probl�mes � l'execution
Un dossier Executable est pr�sent � la racine du projet. Vous pouvez utiliser le run.bat pour lancer le projet