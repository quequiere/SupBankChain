﻿using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using log4net.Core;
using Microsoft.Extensions.DependencyInjection;
using SupBankChain.App.Commands;
using SupBankChain.App.Commands.Factory;
using SupBankChain.Core;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.Network;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Model.Network;

namespace SupBankChain.App
{
    class SupBankApp
    {
        static async Task Main(string[] args)
        {
            //Create a service collect to register components
            IServiceCollection services = new ServiceCollection();


            //Register all component
            //Locals components for this project

            //All globals components
            services.registerAllCoreSingleton();

            //Register all commands
            FactoryCommand.registerCommand(services);
            //services.AddTransient<ListWalletsCommand>();


            //Building container with all components
            var provider = services.BuildServiceProvider();

            ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository(Assembly.GetEntryAssembly())).Root.Level = Level.Debug;

            int port = NodeEntity.defaultPort;
            //In case of specific port
            if (args.Length > 0)
            {
                port = int.Parse(args[0]);
                SupLogger.log.Debug("Starting SupBank on custom port: "+port);
            }


            bool bankmod = false;

            if (args.Length > 1)
            {
                if(args[1]=="bankmod")
                {
                    bankmod = true;
                }
                else if(args[1] == "clientmod")
                {
                    //do nothing
                }
                else
                {
                    SupLogger.log.Error("Error, second parameter can be 'bankmod' or 'clientmod'");
                }
                
            }

            //Start all init of SupbankChain core
            await InitializationHelper.onPostInit(provider, port,bankmod);

            byte[] inputBuffer = new byte[1024];
            Stream inputStream = Console.OpenStandardInput(inputBuffer.Length);
            Console.SetIn(new StreamReader(inputStream, Console.InputEncoding, false, inputBuffer.Length));
       

            while (true)
            {
                Console.WriteLine("Please provide your command");

                //Wait for user input
                String commandInput = Console.ReadLine();

                //Try to get the command with his name
                ICommand command = FactoryCommand.createCommand(provider, commandInput);

                if(command!=null)
                    await command.runCommandAsync();
                 
            }

        }


    }
}
