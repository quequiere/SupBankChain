﻿using System;
using System.Threading.Tasks;
using SupBankChain.App.Commands.Factory;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.App.Commands
{
    [CommandName("createwallet")]
    public class CreateWalletCommand : ICommand
    {
        private readonly IProfileComponent profile;
        private Wallet walletAddress;

        public CreateWalletCommand(IProfileComponent profile)
        {
            this.profile = profile;
        }

        public Task runCommandAsync()
        {
            try
            {
                walletAddress = profile.createWallet();
                Console.WriteLine("Wallet created with address : " + walletAddress.publicKey);
            }
            catch (Exception e)
            {
                Console.WriteLine("There was a problem to create the wallet: " + e.Message);
            }

            return Task.CompletedTask;
        }
    }
}
