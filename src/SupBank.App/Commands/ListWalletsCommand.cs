﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SupBankChain.App.Commands.Factory;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.App.Commands
{
    [CommandName("listwallet")]
    public class ListWalletsCommand : ICommand
    {
        private IProfileComponent profile;
        private IWalletCompenent walletComponent;

        public ListWalletsCommand(IProfileComponent profile, IWalletCompenent walletComponent)
        {
            this.profile = profile;
            this.walletComponent = walletComponent;
        }

        public Task runCommandAsync()
        {
            ReadOnlyCollection<Wallet> wallets = profile.GetUserWallets();

            if (wallets.Count > 0)
            {
                int i = 0;

                foreach (Wallet w in wallets)
                {
                    double bal = walletComponent.getWalletBalance(w.publicKey);
                    Console.WriteLine($"\n================ {w.walletName} ====================");
                    Console.WriteLine($"Token: { bal } public address bellow\n");
                    Console.WriteLine(w.beautyPublicKey());
                    Console.WriteLine($"\n===================================================");
                }
            }
            else
            {
                Console.WriteLine("Sorry but we can't find wallet, use createwallet command to generate a wallet");
            }

            return Task.CompletedTask;
        }
    }
}
