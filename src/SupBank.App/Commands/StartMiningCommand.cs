﻿using System;
using System.Threading.Tasks;
using SupBankChain.App.Commands.Factory;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;

namespace SupBankChain.App.Commands
{
    [CommandName("mine")]
    class StartMiningCommand : ICommand
    {
        private IMinerComponent miner;

        public StartMiningCommand(IMinerComponent miner)
        {
            this.miner = miner;
        }


        public Task runCommandAsync()
        {

            this.miner.startMiningAsync();


            //End the commands with success
            return Task.CompletedTask;
        }

    }
}
