﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SupBankChain.App.Commands.Factory;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Model.BlockChain;
using WebSocketSharp;

namespace SupBankChain.App.Commands
{
    [CommandName("dotransaction")]
    public class DoTransactionCommand : ICommand
    {
        private readonly ITransactionComponent transactionComponent;
        private readonly IWalletCompenent walletComponent;
        private readonly IProfileComponent profileComponent;
        private double outAmount;
        private int senderWalletID;

        public DoTransactionCommand(ITransactionComponent transaction, IWalletCompenent wallet, IProfileComponent profileComponent)
        {
            this.transactionComponent = transaction;
            this.walletComponent = wallet;
            this.profileComponent = profileComponent;
        }

        public Task runCommandAsync()
        {
            try
            {
                /* Amount input from command user and validation on the typed value */
                bool validEntry = false;

                while (!validEntry)
                {
                    Console.WriteLine("Please enter the amount you wish to send: ");
                    string amount = Console.ReadLine();
                    validEntry = Double.TryParse(amount, out outAmount);

                    if (!validEntry) SupLogger.log.Debug("You must type a numeric value here!");
                    if (outAmount <= 0)
                    {
                        validEntry = false;
                        Console.WriteLine("Your value must be above 0!");
                    }
                }

                /* Retrieve sender wallet from command user with some validation */
                bool validSenderWallet = false;
                ReadOnlyCollection<Wallet> wallets = profileComponent.GetUserWallets();

                // List wallets from user profile
                if (wallets.Count > 0)
                {
                    int i = 0;

                    foreach (Wallet w in wallets)
                    {
                        double bal = walletComponent.getWalletBalance(w.publicKey);
                        Console.WriteLine($"[{i++}] {w.walletName} {w.beautyPublicKey(15)} Token ==> {bal}");
                    }
                }
                else
                {
                    throw new Exception("We can't find any wallet from your profile, use createwallet command first!");
                }

                while (!validSenderWallet)
                {
                    // Now asking for sender wallet
                    Console.WriteLine("Please select the sender wallet below (use identifier): ");

                    string senderAddress = Console.ReadLine();
                    validSenderWallet = int.TryParse(senderAddress, out senderWalletID);

                    if (!validSenderWallet || senderWalletID < 0 || senderWalletID > (wallets.Count - 1))
                    {
                        validSenderWallet = false;
                        Console.WriteLine("You must type a valid wallet identifier from above wallets list!");
                    }
                }

                /* Asking now recipient address from command user */
                Console.WriteLine("Please enter the recipient address: ");
                string receiverAddress = Console.ReadLine();

                Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverAddress);

                /* Do transaction ... */
                transactionComponent.DoTransaction(wallets[senderWalletID], receiver, outAmount);

                // SupLogger.log.Debug("Your amount of " + amount + " have been sent to: " + receiverPublicKey);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            return Task.CompletedTask;
        }

    }
}
