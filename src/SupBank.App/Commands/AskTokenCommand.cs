﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using SupBankChain.App.Commands.Factory;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network.Messaging.Ask;

namespace SupBankChain.App.Commands
{
    [CommandName("asktoken")]
    public class AskTokenCommand : ICommand
    {
        private readonly IProfileComponent profile;
        private readonly IWalletCompenent walletComponent;
 

        public AskTokenCommand(IProfileComponent profile, IWalletCompenent walletComponent)
        {
            this.profile = profile;
            this.walletComponent = walletComponent;
        }

        public Task runCommandAsync()
        {

            if(profile.isBankAccount())
            {
                Console.WriteLine("You can't run this command when you are a bank instance yourself !");
                return Task.CompletedTask;
            }

            bool validEntry = false;
            double askedAmount = 0;

            while (!validEntry)
            {
                Console.WriteLine("This is a debug command to ask to the bank to transfert token to your wallet, never use it in prod");
                Console.WriteLine("Please enter the amount you wish to receive: ");

                string amount = Console.ReadLine();
                validEntry = Double.TryParse(amount, out askedAmount);

                if (!validEntry) Console.WriteLine("You must type a numeric value here!");
                if (askedAmount <= 0)
                {
                    validEntry = false;
                    Console.WriteLine("Your value must be above 0!");
                }
            }



            bool validSenderWallet = false;
            ReadOnlyCollection<Wallet> wallets = profile.GetUserWallets();

            // List wallets from user profile
            if (wallets.Count > 0)
            {
                int i = 0;

                foreach (Wallet w in wallets)
                {
                    double bal = walletComponent.getWalletBalance(w.publicKey);
                    Console.WriteLine($"[{i++}] {w.walletName} {w.beautyPublicKey(15)} Token ==> {bal}");
                }
            }
            else
            {
                Console.WriteLine("We can't find any wallet from your profile, use createwallet command first!");
                return Task.CompletedTask;
            }

            int senderWalletID = -1;
            while (!validSenderWallet)
            {
                // Now asking for sender wallet
                Console.WriteLine("Please select the sender wallet below (use identifier): ");

                string senderAddress = Console.ReadLine();
                validSenderWallet = int.TryParse(senderAddress, out senderWalletID);

                if (!validSenderWallet || senderWalletID < 0 || senderWalletID > (wallets.Count - 1))
                {
                    validSenderWallet = false;
                    Console.WriteLine("You must type a valid wallet identifier from above wallets list!");
                }
            }

            Wallet targetWallet = wallets[senderWalletID];

            string message = new AskTokenFromBank(askedAmount,targetWallet.publicKey).toJson();
            SupNetwork.network.BroadcastCustomMessage(message);

            Console.WriteLine("Token was aked to bank, you should receive your token soon (times to validate next block) if bank as enough token.");

            return Task.CompletedTask;
        }
    }
}
