﻿using System;
using System.Threading.Tasks;

namespace SupBankChain.App.Commands.Factory
{
    public interface ICommand
    {
        /// <summary>
        /// Run the command
        /// </summary>
        /// <returns>Task</returns>
        Task runCommandAsync();

    }

    public class CommandName : Attribute
    {

        public string name { get; }
        public CommandName(string name)
        {
            this.name = name;
        }

    }
}
