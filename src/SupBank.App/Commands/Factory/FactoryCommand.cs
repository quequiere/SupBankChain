using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using SupBankChain.Core.Model.Network.Messaging;

namespace SupBankChain.App.Commands.Factory
{
    public class FactoryCommand
    {
        private static List<Type> allCommands;

        public static ICommand createCommand(IServiceProvider provider, String commandName)
        {
            return GetIcommandByName(provider,commandName);

        }

        public static ICommand GetIcommandByName(IServiceProvider provider, string name)
        {
            Type command = getCommands().FirstOrDefault(t => CustomAttributeExtensions.GetCustomAttribute<CommandName>(t).name.Equals(name));
            if (command != null)
            {
                return (ICommand) provider.GetService(command);
            }
            Console.WriteLine("Can't find command, available commands are:");
            getCommands().ForEach(c => Console.WriteLine($"- {c.GetCustomAttribute<CommandName>().name}"));
                    
            return null;
        }

        public static void registerCommand(IServiceCollection services)
        {
            getCommands().ForEach(c => services.AddTransient(c));
        }

        private static List<Type> getCommands()
        {
            if (allCommands == null)
            {
                allCommands = Assembly.GetAssembly(typeof(ICommand)).GetTypes().Where(t => t.IsClass && !t.IsAbstract && t.GetInterfaces().Contains(typeof(ICommand))).ToList();
            }

            return allCommands;

        }
    }


}
