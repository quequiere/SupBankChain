﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SupBankChain.Core.Components;

namespace SupBankChain.Core.Test
{
    [TestClass]
    public class LoggingSetup
    {
        [AssemblyInitialize]
        public static void Configure(TestContext tc)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());

            log4net.Config.BasicConfigurator.Configure(logRepository,
              new log4net.Appender.ConsoleAppender
              {
                  Layout = new log4net.Layout.SimpleLayout()
              });


            ILog log = LogManager.GetLogger(typeof(LoggingSetup));
            log.Error("Log system enabled");
        }
    }
}
