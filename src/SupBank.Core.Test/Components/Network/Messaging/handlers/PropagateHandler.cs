﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SupBankChain.Core.Components.Network;
using SupBankChain.Core.Components.Network.Messaging.handlers;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network;
using SupBankChain.Core.Tools;

namespace SupBankChain.Core.Test.Components.Network.Messaging.handlers
{
    [TestClass]
    public class PropagateHandlerTest
    {
        [TestMethod]
        [Description("Check that the hash of the peer list is equal when the origin is set, or not")]
        public void GetSignedPeerListAndRemoveOriginTest()
        {
            String ownIp = "111.111.111.110";
            int ownPort = 7575;
            NodeEntity ownNode = new NodeEntity(ownIp,ownPort);


            Mock<INetworkComponent> mock = new Mock<INetworkComponent>();
            SupNetwork.network = mock.Object;

            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity(ownIp,ownPort),
                new NodeEntity("111.111.111.111",7575),
                new NodeEntity("111.111.111.112",7575)
            });


            Hash resultSignatureWithIgnore = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);

            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity("111.111.111.111",7575),
                new NodeEntity("111.111.111.112",7575)
            });

            Hash resultSignatureWithOutIgnore = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);

            Assert.AreEqual(resultSignatureWithIgnore.transformToString(), resultSignatureWithOutIgnore.transformToString());
        }


        [TestMethod]
        [Description("Check that two different list provide different hash")]
        public void GetDifferentSignature()
        {

            NodeEntity ownNode = new NodeEntity("", 0);


            Mock<INetworkComponent> mock = new Mock<INetworkComponent>();
            SupNetwork.network = mock.Object;

            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity("111.111.111.111",7575),
                new NodeEntity("111.111.111.112",7575)
            });

            Hash firstSignature = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);
        


            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity("111.111.111.111",7575),
                new NodeEntity("111.111.111.113",7575)
            });

            Hash secondSignature = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);
        

            Assert.AreNotEqual(firstSignature.transformToString(),secondSignature.transformToString());
        }



        [TestMethod]
        [Description("Should return null result with different parameters")]
        public void GetNullResult()
        {

            NodeEntity ownNode = new NodeEntity("111.111.111.111", 7575);

            Mock<INetworkComponent> mock = new Mock<INetworkComponent>();
            SupNetwork.network = mock.Object;

            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity("111.111.111.111",7575),
            });
            Hash firstArray = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);
        
            Assert.IsNull(firstArray);


            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
            });

            Hash secondArray = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);

            Assert.IsNull(secondArray);
        }


        [TestMethod]
        [Description("Should provide the same hash when list is not ordered")]
        public void orderedSignature()
        {
            NodeEntity ownNode = new NodeEntity("", 0);


            Mock<INetworkComponent> mock = new Mock<INetworkComponent>();
            SupNetwork.network = mock.Object;

            //good order and good hash
            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity("192.168.0.1",7575),
                new NodeEntity("200.585.4.8",6060),
                new NodeEntity("200.585.4.8",7575)
            });
            Hash firstSignature = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);


            //wrong order but good hash
            mock.Setup(m => m.getConnectedPeers()).Returns(new List<NodeEntity>()
            {
                new NodeEntity("200.585.4.8",7575),   //3
                new NodeEntity("200.585.4.8",6060), //2
                new NodeEntity("192.168.0.1",7575)  //1

            });

            Hash secondSignature = PropagateHandler.getSignedPeerListAndRemoveOrigin(ownNode);
          

            Assert.AreEqual(firstSignature.transformToString(), secondSignature.transformToString());
        }
    }
}
