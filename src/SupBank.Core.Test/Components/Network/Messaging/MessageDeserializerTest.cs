﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SupBankChain.Core.Components.Network.Messaging;
using SupBankChain.Core.Model.Network.Messaging;
using SupBankChain.Core.Model.Network.Messaging.Ask;

namespace SupBankChain.Core.Test.Components.Network.Messaging
{
    [TestClass]
    public class MessageDeserializerTest
    {
        [TestMethod]
        public void ConvertJsonMessageTest()
        {
            AskPeerListSignature listSignatureTyped = new AskPeerListSignature();

            String json = listSignatureTyped.toJson();
            String expectedJson = "{\"MessageType\":\"AskPeerListSignature\"}";

            P2PMessage p2PMessage = MessageDeserializerService.ConvertJsonToP2pMessage(json);

            Assert.IsInstanceOfType(p2PMessage, typeof(AskPeerListSignature));
            Assert.AreEqual(json, expectedJson);
        }
    }
}
