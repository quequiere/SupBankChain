﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.BlockChain.blochainCompo;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class MinerComponentTest
    {

        private static readonly Mock<IBlockChainComponent> chain = new Mock<IBlockChainComponent>();
        private static readonly Mock<ITransactionPoolComponent> pool = new Mock<ITransactionPoolComponent>();
        private static readonly Mock<IBlockchainValidator> validator = new Mock<IBlockchainValidator>();
        private static readonly Mock<IProfileComponent> profile = new Mock<IProfileComponent>();

        readonly MinerComponent subject = new MinerComponent(pool.Object,chain.Object,validator.Object,profile.Object);

        //READ THIS !!!! ==> This function is only to test the REAL solve of a block, this test can take very long time to be solved !
        //[TestMethod]
        public void TestBlockCreation()
        {
            SupLogger.log.Debug("Start very long test, this shouldn't be used ! Except with debug");

            Block testLastBlock = createTestBlock(DateTimeOffset.Now.ToUnixTimeMilliseconds());
            Block currentBlock = new Block(testLastBlock.blockHash, new List<Transaction>(),0);

            subject.generateBlockHash(currentBlock, testLastBlock);
        }

        [TestMethod]
        public void TestgetDifficulty()
        {
            Assert.AreEqual(6, BlockChainUtils.getDifficulty(60 * 1000, 0, 5));

            Assert.AreEqual(4, BlockChainUtils.getDifficulty(60 * 1000 * 11, 0, 5));

            Assert.AreEqual(1, BlockChainUtils.getDifficulty(0, 0, 0));
        }

        [TestMethod]
        public void TestcontainsOnly()
        {
            Assert.IsTrue(subject.containsOnly("0", "0"));
            Assert.IsTrue(subject.containsOnly("0","0000000"));
            Assert.IsTrue(subject.containsOnly("A", "AAAAAAAAA"));


            Assert.IsFalse(subject.containsOnly("0", "00e"));
            Assert.IsFalse(subject.containsOnly("0", "e05b95072e7d45a37828fc2c1c9a8a577b7186dca2af20859a135381fbab148a"));
            Assert.IsFalse(subject.containsOnly("A", "aaaaaaa"));
            Assert.IsFalse(subject.containsOnly("0", "65565"));
            Assert.IsFalse(subject.containsOnly("0", "aze5ez5$"));
            Assert.IsFalse(subject.containsOnly("0", null));
        }

 

        public Block createTestBlock(long timeCreated)
        {
            Block b = new Block(new Hash("Previous-Block-Hash"), new List<Transaction>(),0);
            b.setup(timeCreated, new Hash("Current-Block-Hash"), 1, 0,"");
            return b;
        }

      
    }
}