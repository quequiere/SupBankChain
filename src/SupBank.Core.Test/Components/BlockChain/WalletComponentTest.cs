﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.BlockChain.blochainCompo;
using SupBankChain.Core.Components.Configs;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class WalletComponentTest
    {

        static IRsaComponent generator = new RsaComponent();


        private static readonly Mock<ITransactionPoolComponent> pool = new Mock<ITransactionPoolComponent>();
      
        static IGenesisBlockComponent genesis = new GenesisBlockComponent();
        
       
        

        static Mock<IBlockChainConfigComponent> configChain = new Mock<IBlockChainConfigComponent>();
        static Mock<IConfigSaverComponent> configSaver = new Mock<IConfigSaverComponent>();

        static IProfileComponent profile = new ProfileComponent(generator,configSaver.Object);
        static IBlockChainComponent blockChainComponent = new BlockChainComponent(genesis, configChain.Object);

        static WalletComponent subject = new WalletComponent(generator, blockChainComponent, pool.Object);

        [TestMethod]
        public void testWalletBalance()
        {
            Wallet bruno = profile.GenerateWallet();
            Wallet matthieu = profile.GenerateWallet();
            Wallet fred = profile.GenerateWallet();

            Transaction t1 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput(bruno.publicKey, 5),
                new TransactionOutput(matthieu.publicKey, 5)
            });
            t1.setTransactionInput(new TransactionInput(0, bruno.publicKey, 10, ""));


            Transaction t2 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput(matthieu.publicKey, 4),
                new TransactionOutput(bruno.publicKey, 1)
            });
            t2.setTransactionInput(new TransactionInput(111, matthieu.publicKey, 5, ""));

            Transaction t3 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput(matthieu.publicKey, 1),
                new TransactionOutput(fred.publicKey, 3)
            });
            t3.setTransactionInput(new TransactionInput(222, matthieu.publicKey, 4, ""));


            Transaction t4 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput(matthieu.publicKey, 0),
                new TransactionOutput(bruno.publicKey, 1)
            });
            t4.setTransactionInput(new TransactionInput(333, matthieu.publicKey, 1, ""));

            BlockChainComponent bc = blockChainComponent as BlockChainComponent;
            bc.blocks = new List<Block>();

            bc.blocks.Add(new Block(null, new List<Transaction> { t1 },0));
            bc.blocks.Add(new Block(null, new List<Transaction> { t2 },1));
            bc.blocks.Add(new Block(null, new List<Transaction> { t3 },2));
            bc.blocks.Add(new Block(null, new List<Transaction> { t4 },3));

            Assert.AreEqual(7, subject.getBlockChainBalance(bruno.publicKey,long.MaxValue));
            Assert.AreEqual(3, subject.getBlockChainBalance(fred.publicKey, long.MaxValue));
            Assert.AreEqual(0, subject.getBlockChainBalance(matthieu.publicKey, long.MaxValue));

        }


    }
}
