﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.BlockChain.blochainCompo;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class BlockchainValidatorTest
    {
        static Mock<ITransactionComponent> transactionMock = new Mock<ITransactionComponent>();

        private static BlockchainValidator subject = new BlockchainValidator(transactionMock.Object);


        [DataRow(true)]
        [DataRow(false)]
        [DataTestMethod]
        public void verifyCorrectHashTest(bool expected)
        {
            Block current = new Block(new Hash("DONT-CARE"), new List<Transaction>(),0);
            current.setup(0, new Hash("this"), 1, 21,"");

            if (expected)
                current.setup(0, BlockChainUtils.hashBlock(current), 1, 21,"");

            Assert.AreEqual(expected, subject.verifyCorrectHash(current));
        }

        [DataRow(true)]
        [DataRow(false)]
        [DataTestMethod]
        public void verifyMatchSignatureTest(bool expected)
        {
            Hash previousHash = new Hash("validationHash");

            Block prev = new Block(null, null,0);
            prev.setup(0, previousHash, 0, 0,"");

            Block current;

            if (expected)
            {
                current = new Block(previousHash, null,0);
            }
            else
            {
                current = new Block(new Hash("failHash"), null,0);
            }

            Assert.AreEqual(expected, subject.verifyMatchSignature(current, prev));
        }


        //Right data
        [DataRow(10 * 1000*60, 3, true)]
        [DataRow(70 * 1000*60, 1, true)]
        //difficulty can't be equal
        [DataRow(10 * 1000*60, 2, false)]
        [DataRow(70 * 1000*60, 2, false)]
        //difficulty can't be low or higher
        [DataRow(10 * 1000*60, 1, false)]
        [DataRow(70 * 1000*60, 3, false)]
        [DataTestMethod]
        public void verifyMatchDifficulty(long newTime, int newDifficulty, bool expectedResult)
        {
            Block prev = new Block(null, null,0);
            prev.setup(0, null, 2, 0,"");

            Block current = new Block(null, null,0);
            current.setup(newTime, null, newDifficulty, 0,"");

            Assert.AreEqual(expectedResult, subject.verifyCorrectDifficulty(current, prev));
        }
    }
}