﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.BlockChain.blochainCompo;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class BlockChainComponentTest
    {
        private static readonly Mock<IGenesisBlockComponent> genesis = new Mock<IGenesisBlockComponent>();
        private static readonly Mock<IBlockChainConfigComponent> config = new Mock<IBlockChainConfigComponent>();
   


        BlockChainComponent subject = new BlockChainComponent( genesis.Object,config.Object);


        [TestMethod]
        public void verrifyAllBlockChain()
        {
            List<Block> l = new List<Block>();

            Block prev = new Block(null, null,0);
            prev.setup(0, new Hash("validationHash"), 2, 0,"");
            prev.isGenesisBlock = true;

            l.Add(prev);

        }

        public Block createTestBlock(long timeCreated)
        {
            Block b = new Block(new Hash("Previous-Block-Hash"), new List<Transaction>(),0);
            b.setup(timeCreated, new Hash("Current-Block-Hash"), 1, 0,"");
            return b;
        }

    }
}
