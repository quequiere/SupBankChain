﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NETCore.Encrypt.Internal;
using Newtonsoft.Json;
using SupBankChain.Core.Components.BlockChain;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class RsaComponentTest
    {
        RsaComponent subject = new RsaComponent();

        [TestMethod]
        public void testKeyCreation()
        {
            RSAKey key = subject.generateKeyPair();

            Assert.IsNotNull(key);
            Assert.IsNotNull(key.PrivateKey);
            Assert.IsNotNull(key.PublicKey);
        }

        [TestMethod]
        public void testSignature()
        {
            RSAKey key = subject.generateKeyPair();
            string messageToSign = "test";
            string signedMessage = subject.signData(messageToSign, key.PrivateKey);

            Assert.IsTrue(subject.verifySigned(messageToSign,signedMessage,key.PublicKey));
            Assert.IsFalse(subject.verifySigned("teste",signedMessage,key.PublicKey));


            RSAKey key3 = subject.generateKeyPair();
            string fakeSigned = subject.signData(messageToSign, key3.PrivateKey);


            Assert.IsFalse(subject.verifySigned(messageToSign, fakeSigned, key.PublicKey));
        }



        [TestMethod]
        public void testNotDoubleKey()
        {
            RSAKey key = subject.generateKeyPair();
            RSAKey key2 = subject.generateKeyPair();

            Assert.AreNotEqual(key.PublicKey, key2.PublicKey);
            Assert.AreNotEqual(key.PrivateKey, key2.PrivateKey);
        }

        [TestMethod]
        public void testKeyEncryption()
        {
            RSAKey key = subject.generateKeyPair();
            string originalMessage = "Hello ! I'm bruno !@&éàç*\n yo";

            string cryptedMessage = subject.encryptMessage(key.PublicKey, originalMessage);
            string cryptedMessage2 = subject.encryptMessage(key.PublicKey, originalMessage);

            string decryptedMessage = subject.decryptMessage(key.PrivateKey, cryptedMessage);
            string decryptedMessage2 = subject.decryptMessage(key.PrivateKey, cryptedMessage2);

            Assert.AreNotEqual(cryptedMessage, cryptedMessage2);

            Assert.AreEqual(decryptedMessage, originalMessage);
            Assert.AreNotEqual(cryptedMessage, originalMessage);

            Assert.AreEqual(decryptedMessage2, originalMessage);
            Assert.AreNotEqual(cryptedMessage2, originalMessage);

        }
    }
}
