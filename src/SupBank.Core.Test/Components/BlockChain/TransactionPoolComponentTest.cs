﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NETCore.Encrypt.Internal;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using SupBankChain.Core.Components.BlockChain.blochainCompo;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class TransactionPoolComponentTest
    {
        
        static IRsaComponent rsaComponent = new RsaComponent();

 
        private static readonly Mock<ITransactionPoolComponent> pool = new Mock<ITransactionPoolComponent>();

        private static readonly Mock<IBlockChainConfigComponent> config = new Mock<IBlockChainConfigComponent>();


        static IGenesisBlockComponent genesisBlockComponent = new GenesisBlockComponent();
        private static Mock<IBlockchainValidator> validatorMock = new Mock<IBlockchainValidator>();
        static IBlockChainComponent blockChainComponent = new BlockChainComponent(genesisBlockComponent,config.Object);
        static IWalletCompenent walletComponent = new WalletComponent(rsaComponent, blockChainComponent, pool.Object);


        [TestMethod]
        public void testReadOnlyList()
        {
             ITransactionPoolComponent subject = new TransactionPoolComponent();
            Assert.IsInstanceOfType(subject.getAllPoolVerifiedTransaction(), typeof(IReadOnlyCollection<Transaction>));
        }

        [TestMethod]
        public void testSuccessTransactionAdd()
        {
            ITransactionPoolComponent subject = new TransactionPoolComponent();


            Transaction t = new Transaction(new List<TransactionOutput>());
            t.setTransactionInput(new TransactionInput(0, "brunoAddress", 500, ""));


            Assert.AreEqual(0, subject.getAllPoolVerifiedTransaction().Count);

            subject.addTransactionToQueue(t);

            Assert.AreEqual(1, subject.getAllPoolVerifiedTransaction().Count);

            Assert.ThrowsException<Exception>(() => subject.addTransactionToQueue(t));

        }


        [TestMethod]
        public void testclearTransactionAlreadyRegistred()
        {
            ITransactionPoolComponent subject = new TransactionPoolComponent();

            Transaction t1 = new Transaction(new List<TransactionOutput>());
            t1.setTransactionInput(new TransactionInput(0, "brunoAddress", 500, ""));

            Transaction t2 = new Transaction(new List<TransactionOutput>());
            t2.setTransactionInput(new TransactionInput(0, "brunoAddress", 500, ""));

            Transaction t3 = new Transaction(new List<TransactionOutput>());
            t3.setTransactionInput(new TransactionInput(0, "brunoAddress", 500, ""));


            List<Transaction> tlist = new List<Transaction>
            {
                t1,t3
            };
            Block b = new Block(null,tlist,0);

            subject.addTransactionToQueue(t1);
            subject.addTransactionToQueue(t2);
            subject.addTransactionToQueue(t3);

            subject.clearTransactionAlreadyRegistred(b);

            Assert.AreEqual(1, subject.getAllPoolVerifiedTransaction().Count);
            Assert.IsTrue(subject.getAllPoolVerifiedTransaction().Any(t => t.id == t2.id));
            Assert.IsFalse(subject.getAllPoolVerifiedTransaction().Any(t => t.id == t3.id));
            Assert.IsFalse(subject.getAllPoolVerifiedTransaction().Any(t => t.id == t1.id));
        }


        [TestMethod]
        public void hasBalanceNotWritedTest()
        {
            ITransactionPoolComponent subject = new TransactionPoolComponent();

            Transaction t1 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput("brunoAddress",490),
                new TransactionOutput("mingAddress",10)
            });
            t1.setTransactionInput(new TransactionInput(0,"brunoAddress",500,""));

            Transaction t2 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput("mingAddress",10)
            });
            t2.setTransactionInput(new TransactionInput(1, "No", -9999, ""));

            Transaction t3 = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput("MatthieuAddress",10),
                new TransactionOutput("brunoAddress",5),
            });
            t3.setTransactionInput(new TransactionInput(2, "No", -9999, ""));

            subject.addTransactionToQueue(t1);
            subject.addTransactionToQueue(t2);
            subject.addTransactionToQueue(t3);

            double balance = -1;
            double balance2 = -1;
            double balance3 = -1;

            bool result1 = subject.getPendingBalance(out balance, "brunoAddress");
            Assert.IsTrue(result1);
            Assert.AreEqual(495, balance);

            bool result2 = subject.getPendingBalance(out balance2, "mingAddress");
            Assert.IsFalse(result2);
            Assert.AreEqual(20, balance2);

            bool result3 = subject.getPendingBalance(out balance3, "ANOTHERADDRESS");
            Assert.IsFalse(result3);
            Assert.AreEqual(0, balance3);
        }


    }
}
