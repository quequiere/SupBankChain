﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NETCore.Encrypt.Internal;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Newtonsoft.Json;

namespace SupBankChain.Core.Test.Components.BlockChain
{
    [TestClass]
    public class TransactionComponentTest
    {
        static IRsaComponent rsaCompo = new RsaComponent();

        static Mock<IRsaComponent> mockRsa = new Mock<IRsaComponent>();

        static ITransactionPoolComponent _transactionPoolComponent = new TransactionPoolComponent();


        static Mock<IWalletCompenent> walleComponentMock = new Mock<IWalletCompenent>();

        static ITransactionComponent subject =
            new TransactionComponent(mockRsa.Object, _transactionPoolComponent, walleComponentMock.Object);

        [TestMethod]
        public void testTransactionCreation()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);
            walleComponentMock.Setup(w => w.getWalletBalance(sender.publicKey,-1)).Returns(50);

            mockRsa.Setup(s => s.verifySigned(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            Transaction result = subject.DoTransaction(sender, receiver, 10);

            double totalMoney = result.outputs.Select(o => o.amount).Sum();
            Assert.AreEqual(totalMoney, 50);

            Assert.AreEqual(result.outputs[0].amount, 10);
            Assert.AreEqual(result.outputs[1].amount, 40);
        }

        [TestMethod]
        public void testFailedTransactionAmount()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);
            walleComponentMock.Setup(w => w.getBlockChainBalance(sender.publicKey,-1)).Returns(50);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            Transaction result = subject.DoTransaction(sender, receiver, 99999);

            Assert.IsNull(result);
        }


        [TestMethod]
        public void generateTransactionSignatureTest()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);
            walleComponentMock.Setup(w => w.getWalletBalance(sender.publicKey,-1)).Returns(50);

            mockRsa.Setup(s => s.verifySigned(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            Transaction result = subject.DoTransaction(sender, receiver, 10);
            TransactionInput inputTransaction = result.GetInput();

            Assert.AreEqual(sender.publicKey, inputTransaction.originPublicAddress);
        }

        [TestMethod]
        public void verifyGoodTransaction()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);
            walleComponentMock.Setup(w => w.getWalletBalance(sender.publicKey,-1)).Returns(50);

            mockRsa.Setup(r => r.verifySigned(It.IsAny<String>(), It.IsAny<String>(), It.IsAny<String>()))
                .Returns(true);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            Transaction result = subject.DoTransaction(sender, receiver, 10);

            Assert.IsTrue(subject.verifyTransaction(result));
        }


        [TestMethod]
        public void verifyFakeTransaction()
        {
            ITransactionComponent unmockSubject =
                new TransactionComponent(rsaCompo, _transactionPoolComponent, walleComponentMock.Object);

            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);
            walleComponentMock.Setup(w => w.getWalletBalance(sender.publicKey,-1)).Returns(50);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            RSAKey tiers = rsaCompo.generateKeyPair();


            Transaction result = unmockSubject.DoTransaction(sender, receiver, 10);


            result.outputs[0].publicAddress = tiers.PublicKey;

            Assert.IsFalse(unmockSubject.verifyTransaction(result));
        }


        [TestMethod]
        public void verifyHackedSumTransaction()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);
            walleComponentMock.Setup(w => w.getWalletBalance(sender.publicKey,-1)).Returns(50);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            Transaction result = subject.DoTransaction(sender, receiver, 10);

            result.outputs[0].amount = 99999;

            Assert.IsFalse(subject.verifyTransaction(result));
        }


        [TestMethod]
        public void failToVerifySum()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            mockRsa.Setup(r => r.verifySigned(It.IsAny<String>(), It.IsAny<String>(), It.IsAny<String>()))
                .Returns(true);

            Transaction transaction = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput(sender.publicKey, 5),
                new TransactionOutput(receiver.publicKey, 999999)
            });
            transaction.setTransactionInput(new TransactionInput(0, sender.publicKey, 10, ""));


            Assert.IsFalse(subject.verifyTransaction(transaction));
        }


        [TestMethod]
        public void verifyTransactionSerialisation()
        {
            RSAKey senderKey = rsaCompo.generateKeyPair();
            Wallet sender = new Wallet(senderKey.PrivateKey, senderKey.PublicKey);

            RSAKey receiverKey = rsaCompo.generateKeyPair();
            Wallet receiver = new Wallet("NOPE! we don't know his private key", receiverKey.PublicKey);

            Transaction transaction = new Transaction(new List<TransactionOutput>
            {
                new TransactionOutput(sender.publicKey, 5),
                new TransactionOutput(receiver.publicKey, 999999)
            });

            transaction.setTransactionInput(new TransactionInput(0, "niawa", 20, "signed"));

            string serialize = JsonConvert.SerializeObject(transaction);

            Transaction deserialized = JsonConvert.DeserializeObject<Transaction>(serialize);

            Assert.IsTrue(serialize.Contains("input"));
            Assert.IsTrue(serialize.Contains("id"));
            Assert.IsTrue(serialize.Contains("outputs"));
            Assert.IsTrue(serialize.Contains("niawa"));

            Assert.IsNotNull(deserialized.GetInput());
            Assert.AreEqual(deserialized.GetInput().signedOutput, transaction.GetInput().signedOutput);
            Assert.AreEqual(deserialized.id, transaction.id);
        }
    }
}