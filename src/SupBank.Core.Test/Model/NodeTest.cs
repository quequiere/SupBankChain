using Microsoft.VisualStudio.TestTools.UnitTesting;
using SupBankChain.Core.Model.Network;

namespace SupBankChain.Core.Test.Model
{
    [TestClass]
    public class NodeTest
    {
        [TestMethod]
        public void verifyNodeCreationTest()
        {
            string ip = "192.168.0.1";
            NodeEntity n = new NodeEntity(ip);

            Assert.AreEqual(ip,n.ip);
        }
    }
}
