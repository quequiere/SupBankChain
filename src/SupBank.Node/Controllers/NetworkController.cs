﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using SupBankChain.Core.Components.Network;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Model.Network;

namespace SupBankChain.Node.Controllers
{
    [Route("network")]
    public class NetworkController : Controller
    {
        private INetworkComponent networkComponent;

        public NetworkController(INetworkComponent networkComponent)
        {
            this.networkComponent = networkComponent;
            
        }


        /// <summary>
        /// Expose network on web with route http://baseUrl/network
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<String>> Get()
        {
            // Fake data, this data need to be all peers in the network
            //Call network to get the node list
            //TODO: Call the network component to display peers in the network
            List<NodeEntity> peers = SupNetwork.network.getConnectedPeers();

            Console.WriteLine("Total node available: "+ peers.Count);

            List<NodeEntity> nodes = new List<NodeEntity>();
            nodes.Add(networkComponent.getOwnNode());
            // I use NodeEntity to verify the getConnectedPeers return. If it return the default port, it is automaticly not print 
            foreach (NodeEntity peer in peers)
                nodes.Add(peer);

            //return all nodes in the network
            return nodes.Select(it => it.getPrettyPrint()).ToList();
        }

        public IPAddress generateLocalIp()
        {
          
            var httpConnectionFeature = HttpContext.Features.Get<IHttpConnectionFeature>();
            return httpConnectionFeature?.LocalIpAddress;
        }

    }
}
