﻿using Microsoft.AspNetCore.Mvc;
using SupBankChain.Node.Model;

namespace SupBankChain.Node.Controllers
{
    [Route("")]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            IndexModel model = new IndexModel()
            {
                welcomeMessage = "Welcome on a SupBank Node !(1.0)"
            };

            return View(model);
        }

    }
}
