﻿using System;
using Newtonsoft.Json;

namespace SupBankChain.Core.Model.Network.Messaging
{
    public abstract class P2PMessage
    {
        public String MessageType { get;}
        protected P2PMessage()
        {
            this.MessageType = this.GetType().Name;
        }


        public String toJson()
        {
            return JsonConvert.SerializeObject(this);
        }


    }

}
