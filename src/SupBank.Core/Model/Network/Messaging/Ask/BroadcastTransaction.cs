﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SupBankChain.Core.Model.Network.Messaging.Ask
{
    public class BroadcastTransaction : P2PMessage
    {
        public Transaction transaction;

        public BroadcastTransaction(Transaction transaction)
        {
            this.transaction = transaction;
        }
    }
}
