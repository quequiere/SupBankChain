﻿namespace SupBankChain.Core.Model.Network.Messaging.Ask
{
    public class AskTokenFromBank : P2PMessage
    {
        public double askedMoney=-1;
        public string publicAddress = "";

        public AskTokenFromBank()
        {

        }

        public AskTokenFromBank(double askedMoney,string publicAddress)
        {
            this.askedMoney = askedMoney;
            this.publicAddress = publicAddress;
        }
    }
}
