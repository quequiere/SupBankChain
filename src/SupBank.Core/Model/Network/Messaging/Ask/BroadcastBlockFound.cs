﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SupBankChain.Core.Model.Network.Messaging.Ask
{
    public class BroadcastBlockFound : P2PMessage
    {
        public Block block;

        public BroadcastBlockFound(Block block)
        {
            this.block = block;
        }
    }
}
