﻿using System;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Model.Network.Messaging.Response
{
    public class ResponseAskPeerListSignature : P2PMessage
    {
        //This is sha256 signature of peer list
        public Hash PeerListSignatureCode;

        public ResponseAskPeerListSignature(Hash peerListSignature)
        {
            PeerListSignatureCode = peerListSignature;
        }

    }
}
