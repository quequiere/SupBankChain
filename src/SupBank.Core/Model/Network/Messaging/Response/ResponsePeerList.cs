﻿using System.Collections.Generic;

namespace SupBankChain.Core.Model.Network.Messaging.Ask
{
    public class ResponsePeerList : P2PMessage
    {
        public List<NodeEntity> peersList;

        public ResponsePeerList(List<NodeEntity> peersList)
        {
            this.peersList = peersList;
        }
    }
}
