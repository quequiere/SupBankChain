﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SupBankChain.Core.Model.Network.Messaging.Response
{
    class ResponseSendBlockChainState : P2PMessage
    {
        public List<Block> blockcChain;
    }
}
