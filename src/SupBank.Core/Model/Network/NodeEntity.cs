﻿using System;
using Newtonsoft.Json;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.Network.p2p;

namespace SupBankChain.Core.Model.Network
{
    ///
    public class NodeEntity
    {
        public static int defaultPort = 7575;
        public String ip { get; }
        public int port { get; }

        /// <summary>
        /// This is a node on the P2P network
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        [JsonConstructor]
        public NodeEntity(String ip, int port)
        {
            this.ip = ip;

            //In the case where the sender is on the same network
            if (ip.StartsWith("192.168"))
            {
                try
                {
                    //this.ip = SupNetwork.network.getOwnNode().ip;
                    //Nothing special here, we are there when we are on local network
                    this.ip = ip;
                    SupLogger.log.Error("SupBankChain is on local network mode !");
                }
                catch (NullReferenceException e)
                {
                   SupLogger.log.Debug("Can't get ownnode ! This should not happen");
                }
               
            }

            this.port = port;
        }

        public NodeEntity(String ip)
        {
            if (ip.Contains(":"))
            {
                string[] splited = ip.Split(':');
                this.ip = splited[0];
                this.port = int.Parse(splited[1]);
            }
            else
            {
                this.ip = ip;
                this.port = defaultPort;
            }
        
        }

        public String getTotalIp()
        {
            return $"{ip}:{port}";
        }

        /// <summary>
        /// Get ip adress to print.
        /// If the defaultport is used, this port isn't display.
        /// </summary>
        /// <returns></returns>
        public String getPrettyPrint()
        {
            return ((port == defaultPort) ? ip : string.Format("{0}:{1}", ip, port));
        }

        public override string ToString()
        {
            return getTotalIp();
        }

        public bool Equals(object obj)
        {
            if (obj is NodeEntity)
            {
                NodeEntity n = obj as NodeEntity;

                if (this.ip.Equals("127.0.0.1") && this.port.Equals(n.port))
                {
                    return true;
                }
                else if (this.getTotalIp().Equals(n.getTotalIp()))
                {
                    return true;
                }
            }

            return false;
            
        }
    }
}
