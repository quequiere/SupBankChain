﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SupBankChain.Core.Model.BlockChain
{
    public class Wallet
    {
        [JsonProperty]
        public String privateKey { get; }
        [JsonProperty]
        public String publicKey { get; }

        [JsonProperty]
        public String walletName { get; set; }

        public Wallet(string privateKey, string publicKey)
        {
            this.privateKey = privateKey;

            if (!publicKey.ToLower().Contains("modulus"))
            {
                publicKey =
                    "{\"Modulus\":\"PKToReplace\",\"Exponent\":\"AQAB\",\"P\":null,\"Q\":null,\"DP\":null,\"DQ\":null,\"InverseQ\":null,\"D\":null}".Replace("PKToReplace", publicKey);
            }

            this.publicKey = publicKey;
            this.walletName = "unamedWallet";
        }

        [JsonConstructor]
        public Wallet(string privateKey, string publicKey, string name)
        {
            this.privateKey = privateKey;
            this.publicKey = publicKey;
            this.walletName = name;
        }

        override public String ToString()
        {
            return $"Wallet {walletName} public key: {beautyPublicKey()}";
        }

        public string beautyPublicKey(int maxCar = -1)
        {
            JObject json = JObject.Parse(this.publicKey);
            if (maxCar == -1)
                return json["Modulus"].Value<string>();
            else
                return json["Modulus"].Value<string>().Substring(0, maxCar) + "...";
        }
    }
}
