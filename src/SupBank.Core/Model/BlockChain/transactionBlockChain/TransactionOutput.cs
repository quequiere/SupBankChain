﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace SupBankChain.Core.Model.BlockChain
{
    public class TransactionOutput
    {
        [JsonProperty]
        public string publicAddress { get; set; }
        [JsonProperty]
        public double amount { get; internal set; }

        public TransactionOutput(string publicAddress,double amount)
        {
            this.publicAddress = publicAddress;
            this.amount = amount;
        }

    }
}
