﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace SupBankChain.Core.Model.BlockChain
{

    public class Transaction
    {
 
        [JsonProperty]
        public string id { get; }

        [JsonProperty]
        private TransactionInput input { get; set; }

        [JsonProperty]
        public List<TransactionOutput> outputs ;

        public Transaction(List<TransactionOutput> outputs)
        {
            this.id = Guid.NewGuid().ToString();
            this.outputs = outputs;
        }

        [JsonConstructor]
        public Transaction(List<TransactionOutput> outputs, string id, TransactionInput input)
        {
            this.outputs = outputs;
            this.id = id;
            this.input = input;
        }

        public TransactionInput GetInput()
        {
            return this.input;
        }

        //Alias sign transaction
        public void setTransactionInput(TransactionInput transactionInput)
        {
            if (input != null)
                throw new Exception("A transaction can't be signed twice.");

            this.input = transactionInput;

        }
    }
}
