﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace SupBankChain.Core.Model.BlockChain
{
    public class TransactionInput
    {
        [JsonProperty]
        public long createdTime { get; }
        [JsonProperty]
        public string originPublicAddress { get; }
        [JsonProperty]
        public double originBalance { get; internal set; }
        [JsonProperty]
        public string signedOutput { get; }

        public TransactionInput(long createdTime, string originPublicAddress, double originBalance, string signedOutput)
        {
            this.createdTime = createdTime;
            this.originPublicAddress = originPublicAddress;
            this.originBalance = originBalance;
            this.signedOutput = signedOutput;
        }
    }
}
