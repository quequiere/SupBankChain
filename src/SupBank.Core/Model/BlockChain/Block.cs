﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SupBankChain.Core.Model.BlockChain
{
    public class Block
    {
        [JsonProperty]
        public long createdTime { get; set; }

        [JsonProperty]
        public Hash previousBlockHash { get; set; }
        [JsonProperty]
        public Hash blockHash { get; set; }
        [JsonProperty]
        public int difficulty { get; set; }
        [JsonProperty]
        public int nonce { get; set; }
        [JsonProperty]
        public List<Transaction> data { get; set; }
        [JsonProperty]
        public bool isGenesisBlock { get; set; } = false;
        [JsonProperty]
        public string minerWallet { get; set; }

        [JsonProperty]
        public int blockID { get; set; }


        public Block(Hash previousBlockHash, List<Transaction> data,int id)
        {
            this.previousBlockHash = previousBlockHash;
            this.data = data;
            this.blockID = id;
        }

        public void setup(long createdTime, Hash blockHash, int difficulty, int nonce, string publicAddress)
        {
            this.createdTime = createdTime;
            this.blockHash = blockHash;
            this.difficulty = difficulty;
            this.nonce = nonce;
            this.minerWallet = publicAddress;
        }

        public string getDataAsString()
        {
            return JsonConvert.SerializeObject(this.data);
        }
    }
}
