﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace SupBankChain.Core.Model.BlockChain
{
    public class Hash
    {
        [JsonProperty]
        public Byte[] hashed { get; }


        //Really important cause we want to use this constructor to get the real data
        [JsonConstructor]
        public Hash(Byte[] hashed)
        {
            this.hashed = hashed;
        }

        //Hash data and createSignature
        public Hash(string data)
        {
            this.hashed = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(data));
        }

        public void initalizeHashFromHashString(string hash)
        {
            
        }

        public string transformToString()
        {
            if (this.hashed == null)
                return null;

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < this.hashed.Length; i++)
            {
                builder.Append(this.hashed[i].ToString("x2"));
            }

            return builder.ToString();
        }

        public override bool Equals(Object o)
        {
            if (o is Hash)
            {
                if (this.transformToString().Equals((o as Hash).transformToString()))
                    return true;
            }

            return false;
        }

    }
}
