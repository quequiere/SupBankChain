﻿using System;
using System.Collections.Generic;
using System.Text;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Model.Configs
{
    public class UserProfile
    {
        public List<Wallet> wallets { get; set; }

        private string UserName { get; }

        public bool isBankAccount { get; set; }

        public List<string> registredNode{ get; set; }

        public UserProfile(List<Wallet> wallets, string name)
        {
            this.wallets = wallets;
            this.UserName = name;
            this.isBankAccount = false;
            this.registredNode = new List<string>();
        }
    }
}
