﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using NETCore.Encrypt.Internal;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Configs;
using SupBankChain.Core.Components.Configs;
using System.Linq;
using SupBankChain.Core.Model.Network;

namespace SupBankChain.Core.Components.BlockChain
{
    public class ProfileComponent : IProfileComponent
    {
        private static string path = "./config/";
        private static string fileName = "userProfile.json";

        private IRsaComponent rsaGenerator;
        private IConfigSaverComponent configSaverComponent;
        private UserProfile profile;

        public ProfileComponent(IRsaComponent rsaGenerator,IConfigSaverComponent configSaverComponent)
        {
            this.rsaGenerator = rsaGenerator;
            this.configSaverComponent = configSaverComponent;
        }

        public Wallet createWallet()
        {
            Wallet w = GenerateWallet();

            if (this.getUserProfile().wallets == null)
            {
                this.getUserProfile().wallets = new List<Wallet>();
            }

            this.getUserProfile().wallets.Add(w);
            this.saveProfile();
            return w;

        }

        public Wallet GenerateWallet()
        {
            RSAKey key = rsaGenerator.generateKeyPair();
            return new Wallet(key.PrivateKey, key.PublicKey);
        }

        public List<NodeEntity> getLocalNodeEntity()
        {
            return this.getUserProfile().registredNode.Select(n => new NodeEntity(n)).ToList();
        }

        public ReadOnlyCollection<Wallet> GetUserWallets()
        {
            if (this.getUserProfile().wallets == null)
            {
                this.getUserProfile().wallets = new List<Wallet>();
                this.saveProfile();
            }
               
            return this.getUserProfile().wallets.AsReadOnly();
        }

        public bool isBankAccount()
        {
            return this.getUserProfile().isBankAccount;
        }

        public void registerNewPeer(string peerAddress)
        {
            if(!this.getUserProfile().registredNode.Contains(peerAddress))
            {
                this.getUserProfile().registredNode.Add(peerAddress);
                this.saveProfile();
            }
        }

        public void reloadProfile()
        {
            try
            {
                if (configSaverComponent.loadFile(path, fileName, out UserProfile loaded))
                {
                    this.profile = loaded;
                    SupLogger.log.Debug($"Reloaded profile with {profile.wallets.Count} wallets.");
                }
               
            }
            catch(Exception e)
            {
                Console.WriteLine("Fatal error while loading your profiles. Please make a backup of if and remove it if necessary");
                Console.WriteLine("Profile are located in the 'config' folder");
                SupLogger.log.Debug(e.GetType()+" Error while loading profile" + e.Message);

                Console.WriteLine("Press any key to quit");
                Console.WriteLine(e.StackTrace);

                Console.ReadKey();


                throw e;
            }
           
        }

        public void saveProfile()
        {
            configSaverComponent.saveFile(path, fileName, this.getUserProfile());
        }

        public void setBankAccount(bool mode)
        {
            this.getUserProfile().isBankAccount = mode;
            this.saveProfile();
        }

        private UserProfile getUserProfile()
        {
            if (this.profile == null)
            {
                profile = new UserProfile(new List<Wallet>(), "UnamedUser");
                this.saveProfile();
            }

            //Added to support old version of profile.
            if(this.profile.registredNode==null)
            {
                this.profile.registredNode = new List<string>();
                this.saveProfile();
            }

            return profile;
        }

    }

    public interface IProfileComponent
    {
        ReadOnlyCollection<Wallet> GetUserWallets();
        Wallet createWallet();
        Wallet GenerateWallet();
        void saveProfile();
        void reloadProfile();

        bool isBankAccount();
        void setBankAccount(bool mode);

        void registerNewPeer(string peerAddress);

        List<NodeEntity> getLocalNodeEntity();
    }
}
