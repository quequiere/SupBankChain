﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace SupBankChain.Core.Components.Configs
{
    public class ConfigSaverComponent : IConfigSaverComponent
    {
        public bool loadFile<T>(string path, string fileName, out T instance)
        {
            return loadFile(path + fileName,out instance);
        }

        public bool loadFile<T>(string path, out T instance)
        {
            try
            {
                using (StreamReader r = new StreamReader(path ))
                {
                    string json = r.ReadToEnd();
                    instance = JsonConvert.DeserializeObject<T>(json);
                    return true;
                }
            }
            catch (DirectoryNotFoundException e)
            {
                SupLogger.log.Debug("Can't find directory: " + path );
                instance = default(T);
                return false;
            }
            catch (FileNotFoundException e)
            {
                SupLogger.log.Debug("Can't find file: " + path );
                instance = default(T);
                return false;
            }
        }

        public List<T> reloadFolder<T>(string path)
        {
            List<T> list = new List<T>();
            try
            {

                foreach (string completePath in Directory.GetFiles(path))
                {
                    if (loadFile(completePath, out T loaded))
                    {
                        list.Add(loaded);
                    }
                }
            }
            catch (Exception e)
            {
                SupLogger.log.Debug(e.Message);
            }
         

            return list;
        }

        public void removeAllFile(string path)
        {
            foreach (string completePath in Directory.GetFiles(path))
            {
                File.Delete(completePath);
            }
        }

        public void saveFile(string path, string fileName, object data)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            File.WriteAllText(path+fileName, JsonConvert.SerializeObject(data,Formatting.Indented));
        }
    }

    public interface IConfigSaverComponent
    {
        void saveFile(string path, string fileName, Object data);

        bool loadFile<T>(string path, string fileName, out T instance);

        List<T> reloadFolder<T>(string path);
        bool loadFile<T>(string path, out T instance);

        void removeAllFile(string path);

    }
}
