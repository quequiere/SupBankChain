﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SupBankChain.Core.Model.Network.Messaging;
using SupBankChain.Core.Tools;

namespace SupBankChain.Core.Components.Network.Messaging
{
    public class MessageDeserializerService
    {
        private static T ConvertJsonToMessage<T>(String json) where T : P2PMessage
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static P2PMessage ConvertJsonToP2pMessage(String json)
        {
            //Big try to prevent any problem with reflexion
            try
            {
                dynamic jsonObject = JObject.Parse(json);

                String messageType = jsonObject.MessageType;

                if (messageType == null)
                {
                    SupLogger.log.Debug($"Wrong message format, type unknown: {messageType}");
                    return null;
                }


                Type p2PMessage = RelectionFinder.getP2ptypeByName(messageType);

                if (p2PMessage == null)
                {
                    SupLogger.log.Debug($"Can't find a type for message: {messageType}");
                    return null;
                }


                MethodInfo method = typeof(MessageDeserializerService).GetMethod(nameof(MessageDeserializerService.ConvertJsonToMessage), BindingFlags.NonPublic | BindingFlags.Static);

                if (method == null)
                {
                    SupLogger.log.Debug($"Method to execute not found: {nameof(MessageDeserializerService.ConvertJsonToMessage)}");
                    return null;
                }

                method = method.MakeGenericMethod(p2PMessage);

                P2PMessage result = method.Invoke(null, new[] { json }) as P2PMessage;
                return result;
            }
            catch (Exception e)
            {
                SupLogger.log.Debug(e);
                return null;
            }
 

           
        }
    }
}