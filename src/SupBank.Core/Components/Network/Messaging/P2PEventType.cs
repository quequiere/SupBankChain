﻿using System;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Components.Network.p2p.server;
using SupBankChain.Core.Model.Network.Messaging;
using WebSocketSharp;

namespace SupBankChain.Core.Components.Network.Messaging
{
    public class P2PMessageServerEvent : EventArgs
    {
        public IP2PServer server { get; }
        public P2PMessage message { get; }

        public P2PMessageServerEvent(IP2PServer server, P2PMessage message)
        {
            this.server = server;
            this.message = message;
        }
    }

    public class P2PMessageClientEvent : EventArgs
    {
        public IP2PClient client { get; }
        public P2PMessage message { get; }

        public WebSocket sender { get; }

        public P2PMessageClientEvent(IP2PClient client, P2PMessage message, WebSocket sender)
        {
            this.client = client;
            this.message = message;
            this.sender = sender;
        }
    }
}