﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network;
using SupBankChain.Core.Tools;

namespace SupBankChain.Core.Components.Network.Messaging.handlers
{
    public class PropagateHandler
    {

        public static Hash getSignedPeerListAndRemoveOrigin(NodeEntity removeNodeOrigin)
        {
            IEnumerable<String> peers = SupNetwork.network.getConnectedPeers()
                .Where(node => !node.Equals(removeNodeOrigin))
                .Select(node => $"{node.ip}:{node.port}-")
                .OrderBy(it => it);

            if (!peers.Any())
                return null;

            return new Hash(String.Concat(peers));
        }
    }
}
