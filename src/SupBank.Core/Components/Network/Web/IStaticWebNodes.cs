﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SupBankChain.Core.Model.Network;

namespace SupBankChain.Core.Components.Network.Web
{
    public interface IStaticWebNodes
    {
        Task<List<NodeEntity>> getNetworkFromWeb();
    }
}
