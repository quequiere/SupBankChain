﻿using System.Threading.Tasks;

namespace SupBankChain.Core.Components.Network.Web
{
    public interface IWebClient
    {
        Task<string> getStringFromWeb(string url);
    }
}
