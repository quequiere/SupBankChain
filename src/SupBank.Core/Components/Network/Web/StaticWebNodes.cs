﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SupBankChain.Core.Model.Network;
using WebSocketSharp;

namespace SupBankChain.Core.Components.Network.Web
{
    public class StaticWebNodes : IStaticWebNodes
    {
        private IWebClient webClient;

        public StaticWebNodes(IWebClient client)
        {
            webClient = client;
        }
        /// <summary>
        /// Represent all static nodes in the network that expose the network with http.
        /// </summary>
        private String[] staticHost =
        {
            "http://164.132.206.111:8080/network",
            "http://51.38.137.219:8080/network"
        };

        /// <summary>
        /// Call REST api in WebNodes to get all peers in the network
        /// </summary>
        /// <returns></returns>
        public async Task<List<NodeEntity>> getNetworkFromWeb()
        {
            List<NodeEntity> result = new List<NodeEntity>();


            foreach (string httpAdress in staticHost)
            {
                string webResult = await this.webClient.getStringFromWeb(httpAdress);
                if (!webResult.IsNullOrEmpty())
                {
                    List<String> nodesList = JsonConvert.DeserializeObject<List<String>>(webResult);

                    if (nodesList!=null)
                    {
                        foreach (string jsonLine in nodesList)
                        {
                            result.Add(new NodeEntity(jsonLine));
                        }
                          
                    }
                }

            }

            return result;
        }

    }
}
