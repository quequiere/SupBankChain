﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace SupBankChain.Core.Components.Network.Web
{
    public class WebClient : IWebClient
    {
        public async Task<string> getStringFromWeb(string url)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            try
            {
                HttpResponseMessage response = await client.GetAsync("");
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                else
                {
                    SupLogger.log.Debug($"Error while try to use web node: {url}. Maybe down ?");
                    SupLogger.log.Debug($"Details:  {response.StatusCode} ||| { response.ReasonPhrase}");
                    return null;
                }
            }
            catch(HttpRequestException e)
            {
                SupLogger.log.Debug($"Server refused connection for web node : {url}. Maybe down ?");
                return "";
            }
            catch(Exception e)
            {
                SupLogger.log.Debug($"Error {e} for node : {url}. Maybe down ?");
                return "";
            }

        }
    }
}
