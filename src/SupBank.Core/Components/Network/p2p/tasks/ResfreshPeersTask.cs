﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SupBankChain.Core.Model.Network.Messaging.Ask;

namespace SupBankChain.Core.Components.Network.p2p.tasks
{
    public class ResfreshPeersTask
    {
        private readonly INetworkComponent network;

        public ResfreshPeersTask(INetworkComponent networkComponent)
        {
            this.network = networkComponent;
        }

        private void askRefresh()
        {
            network.BroadcastCustomMessage(new AskPeerListSignature().toJson());
        }

        public async Task StartRefreshJob(int secondsBetweenTwoCheck)
        {
            while (true)
            {
                await Task.Delay(1000 * secondsBetweenTwoCheck);
                askRefresh();
            }
        }
    }
}
