﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.BlockChain.blochainCompo;
using SupBankChain.Core.Components.Network.Messaging;
using SupBankChain.Core.Components.Network.Messaging.handlers;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network;
using SupBankChain.Core.Model.Network.Messaging.Ask;
using SupBankChain.Core.Model.Network.Messaging.Response;

namespace SupBankChain.Core.Components.Network.p2p.server
{
    public class ServerMessageHandler
    {

        private IBlockChainComponent blockChain;
        private ITransactionComponent transactionComponent;
        private ITransactionPoolComponent poolComponent;
        private IBlockchainValidator validator;
        private IMinerComponent miner;
        private IProfileComponent profile;
        private IWalletCompenent walletCompenent;

        public ServerMessageHandler(IBlockChainComponent blockChainChainComponent, ITransactionComponent transactionComponent, ITransactionPoolComponent poolComponent, IBlockchainValidator validator, IMinerComponent miner, IProfileComponent profile, IWalletCompenent walletCompenent)
        {
            this.blockChain = blockChainChainComponent;
            this.transactionComponent = transactionComponent;
            this.poolComponent = poolComponent;
            this.validator = validator;
            this.miner = miner;
            this.profile = profile;
            this.walletCompenent = walletCompenent;
        }


        public void ServerMessageReceivedEvent(Object sender, P2PMessageServerEvent e)
        {

            String clientIp = e.server.getContext().UserEndPoint.Address.ToString();

            int port = int.Parse(e.server.getContext().CookieCollection[0].Value);
            NodeEntity client = new NodeEntity(clientIp,port);

            if (e.message is AskPeerListSignature)
            {
                Hash hash = PropagateHandler.getSignedPeerListAndRemoveOrigin(client);
                if (hash?.hashed == null)
                {
                    //SupLogger.log.Debug("Don't send data hashed null");
                    return;
                }

                ResponseAskPeerListSignature response = new ResponseAskPeerListSignature(hash);
                e.server.answer(response.toJson());

                //SupLogger.log.Debug("Peer hashed ==> " + client);
            }
            else if (e.message is AskPeerList)
            {
                List<NodeEntity> listToSend = SupNetwork.network.getConnectedPeers().Where(node => !node.Equals(client)).ToList();

                //SupLogger.log.Debug($"Peer list {listToSend.Count} ==> {client}");

                ResponsePeerList response = new ResponsePeerList(listToSend);
                e.server.answer(response.toJson());
            }
            else if (e.message is BroadcastTransaction)
            {

                BroadcastTransaction message = e.message as BroadcastTransaction;

                if (message.transaction != null)
                {
                    SupLogger.log.Debug("New transaction is comming from network " + message.transaction.id);
                    this.transactionComponent.registerTransaction(message.transaction);
                }

                else SupLogger.log.Debug("A message for transaction broadcast has been received empty");

            }
            else if (e.message is ResponseSendBlockChainState)
            {
                SupLogger.log.Debug("New BLOCKCHAIN is comming from peer");
                ResponseSendBlockChainState message = e.message as ResponseSendBlockChainState;

                message.blockcChain = message.blockcChain.OrderBy(b => b.blockID).ToList();

               
                if (message.blockcChain.Count <= blockChain.GetBlocks().Count)
                {
                    SupLogger.log.Info("Peer block chain refused, we already have and updated blockchain");
                }
                else if (validator.verrifyAllBlock(message.blockcChain))
                {
                    SupLogger.log.Info("New blockchain has downloaded and accepted !");
                    poolComponent.clearTransaction();
                    blockChain.setDirectBlockChain(message.blockcChain);
                }
                else
                {
                    SupLogger.log.Debug("Invalid blockhain from peer has been refused");
                }
            }
            else if (e.message is BroadcastBlockFound)
            {
                SupLogger.log.Debug("New block is comming from peer");
                BroadcastBlockFound message = e.message as BroadcastBlockFound;

                if (message.block == null)
                {
                    SupLogger.log.Debug("Empty message block found");
                }
                else if ((blockChain.getLastBlock().blockID + 1) < message.block.blockID)
                {
                    SupLogger.log.Info("[X Blochain] You blockchain is not up to date. Stop mining and start dowloading a new version, please wait.");
                    this.miner.suspendMingingF();

                    ResponseToAskbockchainUpdated response = new ResponseToAskbockchainUpdated();
                    e.server.answer(response.toJson());
                }
                else if ((blockChain.getLastBlock().blockID + 1) > message.block.blockID)
                {
                    SupLogger.log.Debug("Block come from an too old version of blockchain, refused it !");
                }
                else if (blockChain.GetBlocks().Any(b => b.blockID == message.block.blockID))
                {
                    SupLogger.log.Debug("We already have a block registred with this id.");
                }
                else if (validator.validateBlock(blockChain.getLastBlock(), message.block))
                {
                    blockChain.addBlock(message.block);
                    SupLogger.log.Info("Successfully accepted new block from network");
                    poolComponent.clearTransactionAlreadyRegistred(message.block);
                    miner.cancelMining();

                    //This is to send the block to all peers in the network
                    BroadcastBlockFound retransmit = new BroadcastBlockFound(message.block);
                    SupNetwork.network.BroadcastCustomMessage(retransmit.toJson());
                }
                else
                {
                    SupLogger.log.Info("Failed to verify new block from network");
                }
            }
            else if (e.message is AskTokenFromBank)
            {
                SupLogger.log.Debug("A peer asked transaction of bank");
                if(profile.isBankAccount())
                {
                    SupLogger.log.Info("This instance is a bank instance. So we need to transfert token to this client. This is a functionality for test the blockchain only. Never use it for production.");
                    AskTokenFromBank message = e.message as AskTokenFromBank;
                    if(message.askedMoney<=0 || message.publicAddress == null || message.publicAddress=="")
                    {
                        SupLogger.log.Debug("Bad request, token must be positive number and public address is recipient wallet");
                    }
                    else
                    {
                        Wallet w = profile.GetUserWallets().First(ww => walletCompenent.getBlockChainBalance(ww.publicKey) >= message.askedMoney);
                        if(w == null)
                        {
                            SupLogger.log.Error($"The bank is out of found to give {message.askedMoney} token to the client !");
                        }
                        else
                        {
                            Wallet receiver = new Wallet("unknow", message.publicAddress);
                            SupLogger.log.Info("A client asked token transfer, trying to send it ...");
                            transactionComponent.DoTransaction(w, receiver, message.askedMoney);
                        }
                    }
                    
                }
                else
                {
                    SupLogger.log.Debug("Cancel, you are not a bank instance");
                }
            }
            else
            {
                SupLogger.log.Debug("Message not understand of type: " + e.message);
            }
        }
    }
}
