﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SupBankChain.Core.Model.Network;
using WebSocketSharp.Net.WebSockets;

namespace SupBankChain.Core.Components.Network.p2p.server
{
    public interface IP2PServer
    {
        /// <summary>
        /// Create a server to listen peers on a specific port, 7575 by default
        /// </summary>
        /// <param name="port"></param>
        Task StartServer(int port);

        void answer(String response);

        WebSocketContext getContext();

        NodeEntity getOwnNode();
    }

}
