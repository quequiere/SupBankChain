﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Open.Nat;
using SupBankChain.Core.Components.Network.Messaging;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Model.Network;
using SupBankChain.Core.Model.Network.Messaging;
using WebSocketSharp;
using WebSocketSharp.Server;
using WebSocketContext = WebSocketSharp.Net.WebSockets.WebSocketContext;

namespace SupBankChain.Core.Components.Network.p2p.server
{
    public class P2PServer :  WebSocketBehavior, IP2PServer
    {


        public static WebSocketServer localServer { get; set; }

        public static WebSocketServiceManager webSocketServiceManager { get; set; }

        public static String publicIp {get; set; }

        private ServerMessageHandler serverHandler;

        public P2PServer(ServerMessageHandler messageHanlder)
        {
            this.serverHandler = messageHanlder;

            this.OnPeerMessage += serverHandler.ServerMessageReceivedEvent;
        }

        public P2PServer initalizer()
        {
            return new P2PServer(this.serverHandler);
        }

        public async Task StartServer(int port)
        {
            //create a local server to listen all forward peers
            localServer = new WebSocketServer(port);

            //create a specific channel for data
            Func<P2PServer> initializer = initalizer;
            localServer.AddWebSocketService<P2PServer>("/globalChannel", initializer);

            webSocketServiceManager = localServer.WebSocketServices;

            //Allow port forwarding from router
            await openUPNP(port);

            await Task.Run(() => localServer.Start());
            
            Console.WriteLine ("╔═══════════════════════════════════════════════════════════════╗");
            Console.WriteLine ("║                            Server                             ║");
            Console.WriteLine($"║Local server started and listen on port: { getOwnNode()}");
            Console.WriteLine ("║                                                               ║");
            Console.WriteLine ("╚═══════════════════════════════════════════════════════════════╝");

        }

        public NodeEntity getOwnNode()
        {
            return new NodeEntity(publicIp,localServer.Port);
        }


        /// <summary>
        /// Event handler to fire when message received
        /// </summary>
        public event EventHandler<P2PMessageServerEvent> OnPeerMessage;

        /// <summary>
        /// When message come on server side
        /// </summary>
        /// <param name="e"></param>
        protected override void OnMessage(MessageEventArgs e)
        {
            String data = e.Data;

            P2PMessage message =  MessageDeserializerService.ConvertJsonToP2pMessage(data);

            if (message != null)
            {
                this.OnPeerMessage.Emit<P2PMessageServerEvent>(this, new P2PMessageServerEvent(this, message));
                return;
            }

            SupLogger.log.Debug("Can't read data from a peer: "+data);
        }

        public void answer(String response)
        {
            Send(response);
        }

        public WebSocketContext getContext()
        {
            return Context;
        }

        protected override void OnOpen()
        {
            String ip = Context.UserEndPoint.Address.ToString();
            String port = Context.CookieCollection[0].Value;
            NodeEntity clientNode = new NodeEntity(ip, int.Parse(port));
          

            if (!SupNetwork.network.getConnectedPeers().Any(it => it.Equals(clientNode)))
            {
                SupLogger.log.Debug("A new peer non registered is connected. Initialize connection on it ...");
                SupNetwork.network.connectToNewPeer(clientNode);
            }

            SupLogger.log.Debug($"[Connect Request] {clientNode}");
        }




        /// <summary>
        /// This is a very usefull function, called to open port on client's router through UPNP protocol
        /// </summary>
        /// <param name="port"></param>
        private async Task openUPNP(int port)
        {
            SupLogger.log.Debug("Starting UPNP protocol, this can take few seconds");
            //Discover the network and router
            var discoverer = new NatDiscoverer();

            //Set the time out of the rule
            var cts = new CancellationTokenSource(10000);

            try
            {
                //Get the router from this current device
                var device = await discoverer.DiscoverDeviceAsync(PortMapper.Upnp, cts);

                try
                {
                    //Add a new rule for the router to open port
                    await device.CreatePortMapAsync(new Mapping(Open.Nat.Protocol.Tcp, port, port, "SupBank"));
                }
                catch (MappingException e)
                {
                    SupLogger.log.Warn($"Port {port} is already bind on your router.");
                }


                IPAddress ip = await device.GetExternalIPAsync();
                publicIp = ip.ToString();
            }
            catch (NatDeviceNotFoundException e)
            {
                SupLogger.log.Debug("No device found for Upnp, this mean that you are running SupBank on a server.");
                IPAddress[] ipv4Addresses = Array.FindAll(Dns.GetHostEntry(string.Empty).AddressList,
                    a => a.AddressFamily == AddressFamily.InterNetwork);

                if (ipv4Addresses.Length > 0)
                {
                    publicIp = ipv4Addresses[0].ToString();
                    SupLogger.log.Debug(
                        $"But we found {publicIp} public ip address. If it's correct this program should work properly");
                }
                else
                {
                    SupLogger.log.Debug("We can't found any public address, this can cause some problems");
                }
            }
            
        }
    }
}
