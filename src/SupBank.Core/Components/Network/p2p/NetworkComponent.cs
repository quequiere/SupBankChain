﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Components.Network.p2p.server;
using SupBankChain.Core.Model.Network;
using System.Linq;

namespace SupBankChain.Core.Components.Network
{
    public class NetworkComponent : INetworkComponent
    {
        private readonly IP2PServer p2PServer;
        private readonly IP2PClient p2PClient;

        public NetworkComponent(IP2PClient p2pClient, IP2PServer p2PServer)
        {
            this.p2PClient = p2pClient;
            this.p2PServer = p2PServer;
        }

        public List<NodeEntity> getConnectedPeers()
        {
            return p2PClient.GetAllPeersToNode().ToList();
        }

        public NodeEntity getOwnNode()
        {
            return p2PServer.getOwnNode();
        }


        /// <summary>
        /// Try to connect on the network
        /// </summary>
        public async Task InitializeConnection()
        {
            await p2PClient.AskAllForwardPeers();
        }

        /// <summary>
        /// Initialize the local server of this peer to receive message from everyone
        /// </summary>
        public async Task CreateLocalServer(int port)
        {
            await p2PServer.StartServer(port);
        }

        /// <summary>
        /// Broadcast a message to all connected peers
        /// </summary>
        /// <param name="message"></param>
        public void BroadcastCustomMessage(string message)
        {
           p2PClient.BroadcastCustomMessage(message);
        }

        public void connectToNewPeer(NodeEntity node)
        {
            p2PClient.ConnectToNewPeer(node);
        }
    }


    public interface INetworkComponent
    {
        Task InitializeConnection();

        Task CreateLocalServer(int port);

        void BroadcastCustomMessage(String message);

        List<NodeEntity> getConnectedPeers();

        NodeEntity getOwnNode();

        void connectToNewPeer(NodeEntity node);

    }
}
