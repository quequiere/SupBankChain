﻿using System;
using System.Collections.Generic;
using System.Linq;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.Network.Messaging;
using SupBankChain.Core.Components.Network.Messaging.handlers;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network;
using SupBankChain.Core.Model.Network.Messaging.Ask;
using SupBankChain.Core.Model.Network.Messaging.Response;
using SupBankChain.Core.Tools;

namespace SupBankChain.Core.Components.Network.p2p.client
{
    public class ClientMessageHandler
    {
        private IBlockChainComponent blockChain;

        public ClientMessageHandler(IBlockChainComponent blockChainComponent)
        {
            blockChain = blockChainComponent;
        }

        public  void ClientMessageReceivedEvent(object o, P2PMessageClientEvent e)
        {

            NodeEntity server = new NodeEntity(e.sender.Url.Authority);


            if (e.message is ResponseAskPeerListSignature)
            {

                ResponseAskPeerListSignature response = e.message as ResponseAskPeerListSignature;

                Hash ourHash = PropagateHandler.getSignedPeerListAndRemoveOrigin(server);

                if (ourHash?.hashed != null && ourHash.Equals(response.PeerListSignatureCode))
                {
                    //SupLogger.log.Debug(server+" ==> Signatures are equals, no refresh needed !");
                }
                else
                {
                    //SupLogger.log.Debug($"Ask peer list ==> {server}");
                    e.sender.Send(new AskPeerList().toJson());
                }
            }
            else if (e.message is ResponsePeerList)
            {
                //SupLogger.log.Debug($"{server} ==> Peer list received");

                List<NodeEntity> incomingList = (e.message as ResponsePeerList).peersList;
                List<NodeEntity> ownList = e.client.GetAllPeersToNode().ToList();

                List<NodeEntity> toAdd = new List<NodeEntity>();

                foreach (var incomingNode in incomingList)
                {
                    if (!ownList.Any(n => n.Equals(incomingNode)))
                    {
                        toAdd.Add(incomingNode);
                    }
                }

                toAdd.ForEach(n => e.client.ConnectToNewPeer(n));

            }
            else if (e.message is ResponseToAskbockchainUpdated)
            {
                ResponseSendBlockChainState response = new ResponseSendBlockChainState();
                response.blockcChain = blockChain.GetBlocks();
                e.sender.Send(response.toJson());
            }
            else
            {
                SupLogger.log.Debug("Message not understand of type: " + e.message);
            }
        }
    }
}
