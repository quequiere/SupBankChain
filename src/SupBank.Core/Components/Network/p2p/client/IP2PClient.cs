﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SupBankChain.Core.Model.Network;
using WebSocketSharp;

namespace SupBankChain.Core.Components.Network.p2p.client
{
    public interface IP2PClient
    {
        /// <summary>
        /// Initialize connexion from http web nodes
        /// </summary>
        Task AskAllForwardPeers();

        /// <summary>
        /// Connect to a peer and ping it to check availability
        /// </summary>
        /// <param name="ipAdress"></param>
        Task ConnectToNewPeer(NodeEntity node, bool registerToLocal = true);

        List<WebSocket> GetAllPeers();

        IEnumerable<NodeEntity> GetAllPeersToNode();

        void BroadcastCustomMessage(String message);
    }

}
