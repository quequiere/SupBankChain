﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.Network.Messaging;
using SupBankChain.Core.Components.Network.p2p.server;
using SupBankChain.Core.Components.Network.Web;
using SupBankChain.Core.Model.Network;
using SupBankChain.Core.Model.Network.Messaging;
using SupBankChain.Core.Model.Network.Messaging.Ask;
using WebSocketSharp;
using WebSocketSharp.Net;

namespace SupBankChain.Core.Components.Network.p2p.client
{
    public class P2PClient : IP2PClient
    {
        private IStaticWebNodes staticNodes;

        //All peers where we are connected
        private List<WebSocket> connectedPeers { get; set; } = new List<WebSocket>();

        private ClientMessageHandler messageHander;
        private IProfileComponent profileComponent;

        public P2PClient(IStaticWebNodes staticNodes, ClientMessageHandler clientHandler, IProfileComponent profileComponent)
        {
            this.staticNodes = staticNodes;
            this.messageHander = clientHandler;
            //Handle all message from other peers
            this.OnServerMessage += messageHander.ClientMessageReceivedEvent;
            this.profileComponent = profileComponent;
        }


        public async Task AskAllForwardPeers()
        {
            List<NodeEntity> availableIpsInNetwork = await staticNodes.getNetworkFromWeb();

            foreach (var node in availableIpsInNetwork)
            {
                ConnectToNewPeer(node);
            }

            foreach(NodeEntity localPeer in profileComponent.getLocalNodeEntity())
            {
                if(!availableIpsInNetwork.Any(n => n.Equals(localPeer)))
                {
                    ConnectToNewPeer(localPeer,false);
                }
            }

            SupLogger.log.Debug($"Connection tried for {profileComponent.getLocalNodeEntity().Count} peers in local save");

            Console.WriteLine("╔═══════════════════════════════════════════════════════════════╗");
            Console.WriteLine("║                          Client                               ║");
            Console.WriteLine($"║Connected on network with {connectedPeers.Count} peers");
            Console.WriteLine("║                                                               ║");
            Console.WriteLine("╚═══════════════════════════════════════════════════════════════╝");
        }



        public async Task ConnectToNewPeer(NodeEntity node, bool registerToLocal = true)
        {
            NodeEntity own = SupNetwork.network.getOwnNode();

            //When node want to connect on itself
            if (node.Equals(own))
            {
                SupLogger.log.Debug("Canceled connect on itself");
                return;
            }

            lock (this.connectedPeers)
            {
                if (this.connectedPeers.Any(p => 
                    p.IsAlive && 
                    p.Url.Host.ToString().Equals(own.ip) &&
                    p.Url.Port.Equals(own.port)))
                {

                    SupLogger.log.Info("[X Connect request]" + node);
                    return;
                }

            }



            String localUrl = $"ws://{node.getTotalIp()}/globalChannel";
            //initialize peer connection
            WebSocket forwardPeer = new WebSocket(localUrl);

  
            //Set the local port
            forwardPeer.SetCookie(new Cookie("portOrigin", P2PServer.localServer.Port.ToString()));

            //try to connect on peer
            forwardPeer.Connect();


            if (forwardPeer.Ping())
            {
              

                //register onMessage event from network
                forwardPeer.OnMessage += OnMessage;

                connectedPeers.Add(forwardPeer);

                SupLogger.log.Info($"[+ Connection] {node.getTotalIp()} connectedPeers {this.GetAllPeersToNode().ToList().Count}");

                if (registerToLocal)
                    profileComponent.registerNewPeer(node.getTotalIp());

                forwardPeer.Send(new AskPeerListSignature().toJson());
                return;
            }

            try
            {
                SupLogger.log.Debug("Tunnel closed");
                forwardPeer.Close();
            }
            catch (Exception)
            {
                SupLogger.log.Debug("Failed to close tunel on a peer");
            }


            SupLogger.log.Debug($"Failed to connect on {node.getTotalIp()}");
            return;
        }

        public List<WebSocket> GetAllPeers()
        {
            this.connectedPeers = this.connectedPeers.Where(it => it.IsAlive).ToList();
            return connectedPeers;
        }

        public IEnumerable<NodeEntity> GetAllPeersToNode()
        {
            return this.GetAllPeers().Select(it => new NodeEntity(it.Url.Host.ToString(), it.Url.Port));
        }

        public void BroadcastCustomMessage(string message)
        {
            foreach (WebSocket peer in this.GetAllPeers())
            {
                peer.Send(message);
            }
        }


        /// <summary>
        /// Event handler to fire when message received
        /// </summary>
        public event EventHandler<P2PMessageClientEvent> OnServerMessage;

        /// <summary>
        /// This method is called when another peer (considered like server) answering, we parse these data in a common language
        /// </summary>
        /// <param name="webSocket"></param>
        /// <param name="eventArgs"></param>
        private void OnMessage(object webSocket, MessageEventArgs e)
        {
            String data = e.Data;
            WebSocket sender = webSocket as WebSocket;

            P2PMessage message = MessageDeserializerService.ConvertJsonToP2pMessage(data);

            if (message != null)
            {
                this.OnServerMessage.Emit<P2PMessageClientEvent>(this,new P2PMessageClientEvent(this, message, sender));
                return;
            }

            SupLogger.log.Debug("Can't read data from server: " + data);
        }

    
    }
}
