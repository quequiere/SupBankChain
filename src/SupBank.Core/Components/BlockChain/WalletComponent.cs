﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using NETCore.Encrypt.Internal;
using SupBankChain.Core.Model.BlockChain;

namespace SupBankChain.Core.Components.BlockChain
{
    public class WalletComponent : IWalletCompenent
    {
        private IBlockChainComponent blockChainComponent;
        private ITransactionPoolComponent poolComponent;

        public WalletComponent(IRsaComponent generator, IBlockChainComponent blockChainComponent, ITransactionPoolComponent poolComponent)
        {
            this.blockChainComponent = blockChainComponent;
            this.poolComponent = poolComponent;
        }

        public double getBlockChainBalance(string publicKey, long timeStop)
        {
            if (blockChainComponent.GetBlocks(timeStop) == null || blockChainComponent.GetBlocks(timeStop).Count <= 0)
                throw new Exception("Error in block chain, seems to be empty");

            double balance = 0;
            double startScanTime = 0;

            //Extract all transaction where input contain the public key of the balance
            List<Transaction> transactionsFiltered = blockChainComponent.GetBlocks(timeStop)
                .Where(b => b.data !=null)
                .SelectMany(b => b.data
                    .Where(t => t !=null && t.GetInput()!=null)
                    .Where(t => t.GetInput().originPublicAddress.Equals(publicKey)))
            .ToList();

            //Get the most recent transaction in blockchain
            Transaction lastTransactionInput = transactionsFiltered.OrderByDescending(t => t.GetInput().createdTime).ToList().FirstOrDefault();

            
            if(lastTransactionInput != null)
            {
                startScanTime = lastTransactionInput.GetInput().createdTime;

                TransactionOutput outputTransac = lastTransactionInput.outputs
                    .Where(o => o.publicAddress.Equals(publicKey)).ToList().FirstOrDefault();

                // if no output, that's means that the wallet is empty, so startbalance stay at 0
                if (outputTransac != null)
                {
                    // get the output of the most recent transaction to set de balance amount
                    balance = outputTransac.amount;
                }
            }


            double addition = blockChainComponent.GetBlocks(timeStop)
                .SelectMany(block => block.data)
                .Where(transac => transac!=null && transac.GetInput()!=null && transac.GetInput().createdTime> startScanTime)
                .SelectMany(trans => trans.outputs)
                .Where(o => o.publicAddress.Equals(publicKey))
                .Sum(o => o.amount);

            balance += addition;

            return balance;
        }


        public double getWalletBalance(string publickKey, long timeStop = -1)
        {
            double balanceFromPool = 0;

            //This means that a transaction for this key has already a verified input
            bool setBalanceDirectlyFromPool = poolComponent.getPendingBalance(out balanceFromPool, publickKey);

            if (setBalanceDirectlyFromPool)
                return balanceFromPool;

            double balance = getBlockChainBalance(publickKey, timeStop) + balanceFromPool;

            return balance;
        }
    }

    public interface IWalletCompenent
    {
        double getBlockChainBalance(string publickKey, long timeStop=-1);

        double getWalletBalance(string publickKey,long timeStop = -1);
    }
}
