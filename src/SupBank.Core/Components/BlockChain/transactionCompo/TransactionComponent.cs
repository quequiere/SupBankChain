﻿using Newtonsoft.Json;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network.Messaging.Ask;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace SupBankChain.Core.Components.BlockChain
{
    public class TransactionComponent : ITransactionComponent
    {

        private IRsaComponent rsaComponent;
        private ITransactionPoolComponent transactionPoolComponent;
        private IWalletCompenent walletCompenent;

        public TransactionComponent(IRsaComponent rsaComponent, ITransactionPoolComponent transactionPoolComponent, IWalletCompenent walletCompenent)
        {
            this.rsaComponent = rsaComponent;
            this.transactionPoolComponent = transactionPoolComponent;
            this.walletCompenent = walletCompenent;
        }


        public Transaction DoTransaction(Wallet sender,Wallet receiver, double tokenAmount)
        {
           
            List<TransactionOutput> outputs = new List<TransactionOutput>(){

                new TransactionOutput(receiver.publicKey, tokenAmount),
                new TransactionOutput(sender.publicKey,walletCompenent.getWalletBalance(sender.publicKey) - tokenAmount)
            };

            Transaction transaction = new Transaction(outputs);


            this.generateTransactionSignature(transaction, sender);

            //We add this transaction to our on queue
            if (!this.registerTransaction(transaction))
                return null;

            try
            {
                SupLogger.log.Info("Publish new transaction on network " + transaction.id);
                //We publish the transaction to the network
                string message = new BroadcastTransaction(transaction).toJson();
                SupNetwork.network.BroadcastCustomMessage(message);
            }
            catch(Exception e)
            {
                SupLogger.log.Error("Error while publish transaction: " + transaction);
            }
  

            return transaction;
        }


        // ------------------------------ THIS PART GENERATE INPUT SIGNATURE -------------------------

        public string generateSignedOutput(List<TransactionOutput> outputs, Wallet sender)
        {
            string jsonOuputs = JsonConvert.SerializeObject(outputs);

            string hashedOutputs = new Hash(jsonOuputs).transformToString();

            return rsaComponent.signData(hashedOutputs, sender.privateKey);
     
        }

        public TransactionInput generateTransactionSignature(Transaction transaction, Wallet sender)
        {
            string signedOutput = generateSignedOutput(transaction.outputs, sender);

            TransactionInput transactionInput = new TransactionInput(DateTimeOffset.Now.ToUnixTimeMilliseconds(), sender.publicKey, walletCompenent.getWalletBalance(sender.publicKey), signedOutput);
            transaction.setTransactionInput(transactionInput);

            return transactionInput;
        }

        // ------------------------------------------------------------------------------------------------
     

        public bool verifyTransaction(Transaction transaction, long pastLimit = -1)
        {
            TransactionInput transactionSignature = transaction.GetInput();

            if (transactionSignature == null)
            {
                SupLogger.log.Debug($"Fail to verify transaction {transaction.id}, no input set !");
                return false;
            }

            //special case for rewards
            if (transaction.GetInput().originPublicAddress.Equals("Reward"))
            {
                if (transaction.outputs.Count == 1 && transaction.outputs[0].amount==1)
                {
                    return true;
                }

                SupLogger.log.Debug($"Failed to verify reward for transaction {transaction.id}");
                return false;
            }






            string hashedOutputs = new Hash(JsonConvert.SerializeObject(transaction.outputs)).transformToString();

            bool signatureSuccess= rsaComponent.verifySigned(hashedOutputs, transactionSignature.signedOutput, transactionSignature.originPublicAddress);

            if (!signatureSuccess)
            {
                SupLogger.log.Debug($"Fail to verify transaction signature for transaction id {transaction.id}");
                return false;
            }

            if (transaction.GetInput().originBalance<=0)
            {
                SupLogger.log.Debug($"Transaction failed, input can't be < or = to 0 {transaction.id}");
                return false;
            }


            if (transaction.outputs.Any(t => t.amount < 0))
            {
                SupLogger.log.Debug($"Transaction failed, output can't be < to 0 {transaction.id}");
                return false;
            }


            double gived = transaction.outputs.Sum(t => t.amount);

            if (gived <= 0)
            {
                SupLogger.log.Debug($"Transaction failed, can't be empty {transaction.id}");
                return false;
            }


            double originBalanceInput = transaction.GetInput().originBalance;

            if (gived > originBalanceInput)
            {
                SupLogger.log.Debug($"Transaction failed, not egought token (output check) {transaction.id}");
                return false;
            }


            double readlSenderBalance;

            if(pastLimit>-1)
                readlSenderBalance =  walletCompenent.getWalletBalance(transaction.GetInput().originPublicAddress, pastLimit);
            else
                readlSenderBalance = walletCompenent.getWalletBalance(transaction.GetInput().originPublicAddress);


            double outputSum = transaction.outputs
                .Where(t => t.publicAddress != transaction.GetInput().originPublicAddress).Sum(t => t.amount);


            if (readlSenderBalance < outputSum)
            {
                SupLogger.log.Debug($"Transaction failed, not enought token (origin balance check) {transaction.id}");
                return false;
            }



            SupLogger.log.Debug($"[Verified] Transaction {transaction.id}");

            return true;

        }


        public bool registerTransaction(Transaction transaction)
        {
            if(verifyTransaction(transaction))
            {
                try
                {
                    transactionPoolComponent.addTransactionToQueue(transaction);
                    Console.WriteLine($"[+ Transaction] ==> {transaction.id}");
                    return true;
                }
                catch (Exception e)
                {
                    SupLogger.log.Debug("[- Transaction] ==> " + transaction.id);
                }
                
            } 
            else
            {
                Console.WriteLine($"[X Transaction] ==> {transaction.id}");
            }

            return false;
        }

        public bool verifyReward(Block b)
        {
            foreach (Transaction t in b.data)
            {
                if (t.GetInput().originPublicAddress.Equals("Reward"))
                {
                    if (t.outputs.Count == 1 && t.outputs[0].amount == 1)
                    {
                        if (t.outputs[0].publicAddress == b.minerWallet)
                        {
                            return true;
                        }

                        SupLogger.log.Debug($"Failed to verify public address for reward");
                        return false;
                    }

                    SupLogger.log.Debug($"Failed to verify reward for transaction {t.id} no transaction or amount invalid");
                    return false;
                }
            }
         

            return true;
        }
    }

    public interface ITransactionComponent
    {
        Transaction DoTransaction(Wallet sender, Wallet receiver, double tokenAmount);

        bool verifyTransaction(Transaction transaction, long pastLimit = -1);

        bool registerTransaction(Transaction transaction);

        bool verifyReward(Block b);
    }
}
