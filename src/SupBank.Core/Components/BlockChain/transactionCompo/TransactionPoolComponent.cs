﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Linq;
using SupBankChain.Core.Model.BlockChain;
using SupBankChain.Core.Model.Network.Messaging.Ask;

namespace SupBankChain.Core.Components.BlockChain
{
    public class TransactionPoolComponent : ITransactionPoolComponent
    {
        public List<Transaction> unwritedTransactions = new List<Transaction>();


        //At this point, transaction are already verified
        public void addTransactionToQueue(Transaction transaction)
        {
            if (unwritedTransactions.Any(t => t.id == transaction.id))
                throw new Exception("Transaction added already exist with same id");
            else if (unwritedTransactions.Contains(transaction))
                throw new Exception("Transaction added already exist with same instance");
            else
            {
                unwritedTransactions.Add(transaction);
                SupLogger.log.Debug($"New transaction added to pool, size is now {unwritedTransactions.Count}");
            }
                
        }

        public void clearTransaction()
        {
            this.unwritedTransactions.Clear();
        }

        public void clearTransactionAlreadyRegistred(Block block)
        {
            unwritedTransactions.RemoveAll(t => block.data.Any(t2 => t2.id == t.id));
        }

        public IReadOnlyCollection<Transaction> getAllPoolVerifiedTransaction()
        {
            return unwritedTransactions.Where(t => t.GetInput()!=null).OrderByDescending(t => t.GetInput().createdTime).ToList().AsReadOnly();
        }


        //Return true if balance has an input, and return the pending balance
        public bool getPendingBalance(out double balance, string publicKey)
        {
            bool hasInput = false;
            balance = 0;
           

            IReadOnlyCollection<Transaction> transactions = getAllPoolVerifiedTransaction();

            Transaction lastInput = transactions
                .FirstOrDefault(t => t.GetInput().originPublicAddress.Equals(publicKey));

            double addition = 0;

            if (lastInput != null)
            {
                balance = lastInput.outputs.Find(o => o.publicAddress.Equals(publicKey)).amount;

                addition = transactions.Where(t => t.GetInput().createdTime> lastInput.GetInput().createdTime).ToList()
                    .SelectMany(t => t.outputs)
                    .Where(o => o.publicAddress.Equals(publicKey)).ToList()
                    .Sum(o => o.amount);

                hasInput = true;
            }
            else if(transactions.Any(t => t.outputs.Any(o => o.publicAddress.Equals(publicKey))))
            {

               addition = transactions.SelectMany(t => t.outputs).Where(o => o.publicAddress.Equals(publicKey)).ToList()
                    .Sum(o => o.amount);
            }

            balance += addition;

            return hasInput;
        }
    }

    public interface ITransactionPoolComponent
    {
        void addTransactionToQueue(Transaction t);
        IReadOnlyCollection<Transaction> getAllPoolVerifiedTransaction();
        void clearTransaction();

        void clearTransactionAlreadyRegistred(Block b);

        bool getPendingBalance(out double balance ,string publicKey);
    }
}
