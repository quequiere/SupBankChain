﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using SupBankChain.Core.Model.Network.Messaging.Ask;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Components.BlockChain.blochainCompo;

namespace SupBankChain.Core.Components.BlockChain
{
    public class MinerComponent : IMinerComponent
    {
        ITransactionPoolComponent poolComponent;
        IBlockChainComponent blockChainComponent;
        IBlockchainValidator validator;
        private IProfileComponent profile;

        private bool suspendMining = false;
        private bool cancelMiningBool = false;
        private bool startedMine = false;

        public MinerComponent(ITransactionPoolComponent poolComponent, IBlockChainComponent blockChainComponent,
            IBlockchainValidator validator, IProfileComponent profile)
        {
            this.poolComponent = poolComponent;
            this.blockChainComponent = blockChainComponent;
            this.validator = validator;
            this.profile = profile;
        }

        public async Task startMiningAsync()
        {
            if (profile.GetUserWallets().Count <= 0)
            {
                SupLogger.log.Error("You can't mine without wallet.");
                return;
            }


            //if startMining is called but already running task
            if (this.suspendMining)
            {
                this.resumeMining();
            }
            else
            {
                startedMine = true;
                SupLogger.log.Info("Supbank has start to mine");
                while (true)
                {
                    //We never stop to mine
                    if (!suspendMining)
                    {
                        //If pass here, we start to mine
                        SupLogger.log.Debug("Start to mine a new block");
                        mineNewBlock(blockChainComponent.getLastBlock());
                    }

                    await Task.Delay(2000);
                }
            }
        }


        public Block mineNewBlock(Block lastBlock)
        {
            Block b = new Block(lastBlock.blockHash, null, lastBlock.blockID + 1);
            generateBlockHash(b, lastBlock);
            return b;
        }


        public void generateBlockHash(Block currentBlock, Block lastBlock)
        {
            string previousBlockHash = lastBlock.blockHash.transformToString();

            int nonce = -1;
            int difficulty = -1;
            long createdTime = 0;

            String firstChar = null;
            Hash currentHash = null;

            List<Transaction> lastTransactions = null;

            do
            {
                nonce++;

                createdTime = DateTimeOffset.Now.ToUnixTimeMilliseconds();

                difficulty = BlockChainUtils.getDifficulty(createdTime, lastBlock.createdTime, lastBlock.difficulty);


                List<Transaction> currentTransactions = this.poolComponent.getAllPoolVerifiedTransaction().ToList();
                // Used to modify block only if new transactions has been accepted
                if (lastTransactions == null || lastTransactions.Count != currentTransactions.Count)
                {
                    lastTransactions = currentTransactions;
                    currentBlock.data = currentTransactions;


                    List<TransactionOutput> originOutput = new List<TransactionOutput>
                    {
                        new TransactionOutput(profile.GetUserWallets()[0].publicKey, 1)
                    };

                    Transaction originTransaction = new Transaction(originOutput);
                    originTransaction.setTransactionInput(new TransactionInput(createdTime, "Reward", 0, ""));

                    currentTransactions.Add(originTransaction);
                }


                currentHash = BlockChainUtils.hashAlgo(createdTime, previousBlockHash, currentBlock.getDataAsString(),
                    nonce, profile.GetUserWallets()[0].publicKey);

                firstChar = currentHash.transformToString().Substring(0, difficulty);

                if (cancelMiningBool)
                {
                    cancelMiningBool = false;
                    return;
                }
            } while (!containsOnly("0", firstChar));

            currentBlock.setup(createdTime, currentHash, difficulty, nonce, profile.GetUserWallets()[0].publicKey);


            long diffTime = (createdTime - lastBlock.createdTime) / 1000L;

            Console.WriteLine(
                $"[WIN!] Block mined with difficulty {difficulty} and nonce {nonce} in {diffTime} seconds for {poolComponent.getAllPoolVerifiedTransaction().Count} transactions");

            poolComponent.clearTransaction();

            if (validator.validateBlock(blockChainComponent.getLastBlock(), currentBlock))
            {
                blockChainComponent.addBlock(currentBlock);
                SupLogger.log.Info(
                    $"Block accepted, chain size is now : {blockChainComponent.GetBlocks().Count}, propagation");
                BroadcastBlockFound message = new BroadcastBlockFound(currentBlock);
                SupNetwork.network.BroadcastCustomMessage(message.toJson());
            }
            else
            {
                SupLogger.log.Error(
                    "Failed to verify your own block. All transaction was supposed to be valid, did you add transaction manually ?");
            }
        }


        public bool containsOnly(String needed, String data)
        {
            if (needed == null || data == null)
                return false;
            return data.All(c => needed.Contains(c));
        }

        public void suspendMingingF()
        {
            SupLogger.log.Info("[MINER] Stoped");
            if (startedMine)
                this.suspendMining = true;
        }

        public void resumeMining()
        {
            SupLogger.log.Info("[MINER] Resumed");
            this.suspendMining = false;
        }

        public void cancelMining()
        {
            this.cancelMiningBool = true;
        }
    }

    public interface IMinerComponent
    {
        bool containsOnly(String needed, String data);
        void generateBlockHash(Block currentBlock, Block lastBlock);

        Block mineNewBlock(Block lastBlock);

        Task startMiningAsync();
        void suspendMingingF();
        void resumeMining();
        void cancelMining();
    }
}