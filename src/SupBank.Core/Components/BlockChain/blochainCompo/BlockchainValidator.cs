﻿using System;
using System.Collections.Generic;
using System.Text;
using SupBankChain.Core.Model.BlockChain;
using System.Linq;

namespace SupBankChain.Core.Components.BlockChain.blochainCompo
{
    public class BlockchainValidator : IBlockchainValidator
    {
        private ITransactionComponent transactionComponent;
        public BlockchainValidator(ITransactionComponent transactionComponent)
        {
            this.transactionComponent = transactionComponent;
        }

        public bool validateBlock(Block previousBlock, Block block, bool pastModOn = false)
        {
            if (!verifyCorrectHash(block))
            {
                SupLogger.log.Debug("[- BlockValidation] Fail to verify block hash "+ block.blockID);
                return false;
            }
            else if(!verifyMatchSignature(block,previousBlock))
            {
                SupLogger.log.Debug("[- BlockValidation] Signature between previous block and current block doesn't matching" + block.blockID +" VS "+previousBlock.blockID);
                SupLogger.log.Debug($"[- BlockValidation] {block.previousBlockHash.transformToString()} and {previousBlock.blockHash.transformToString()}");
                return false;
            }
            else if(!verifyCorrectDifficulty(block,previousBlock) )
            {
                SupLogger.log.Debug("[- BlockValidation] Fail to verify difficulty block " + block.blockID);
                return false;
            }
            else if(block.data.Any(t => !transactionComponent.verifyTransaction(t,block.createdTime)))
            {
                SupLogger.log.Debug("[- BlockValidation] Fail to verify transaction " + block.blockID);
                return false;
            }
            else if (!transactionComponent.verifyReward(block))
            {
                SupLogger.log.Debug("[- BlockValidation] Fail to verify reward " + block.blockID);
                return false;
            }

            return true;
        }

        public bool verifyCorrectHash(Block block)
        {
            Hash realHash = BlockChainUtils.hashBlock(block);
            return realHash.transformToString().Equals(block.blockHash.transformToString());
        }

        public bool verifyMatchSignature(Block current, Block last)
        {
            return current.previousBlockHash.transformToString().Equals(last.blockHash.transformToString());
        }

        public bool verifyCorrectDifficulty(Block current, Block last)
        {
            int expectedDifficulty =
                BlockChainUtils.getDifficulty(current.createdTime, last.createdTime, last.difficulty);

            bool result = current.difficulty == expectedDifficulty;

            if (!result)
            {
                SupLogger.log.Debug($"Error while check difficulty, current {current.difficulty} expected: {expectedDifficulty}");
            }

            return result;
        }

        public bool verrifyAllBlock(List<Block> blocks)
        {
            for (int x = 0; x < blocks.Count; x++)
            {
                Block currentBlock = blocks[x];
                if (x == 0)
                {
                    if (!currentBlock.isGenesisBlock)
                    {
                        SupLogger.log.Debug("First block is not genesis !");
                        return false;
                    }
                    continue;
                }

                Block lastBlock = blocks[x - 1];

              

                if (!this.validateBlock(lastBlock, currentBlock,true))
                {
                    SupLogger.log.Debug($"Block {x} is not a valid block !");
                    return false;
                }

            }
            return true;
        }
    }

    public interface IBlockchainValidator
    {
        bool validateBlock(Block previousBlock, Block block, bool pastModOn = false);
        bool verifyCorrectHash(Block block);
        bool verifyMatchSignature(Block current, Block last);
        bool verifyCorrectDifficulty(Block current, Block last);
        bool verrifyAllBlock(List<Block> blocks);
    }
}
