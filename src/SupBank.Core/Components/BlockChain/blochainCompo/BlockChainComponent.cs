﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SupBankChain.Core.Components.BlockChain.blochainCompo;

namespace SupBankChain.Core.Components.BlockChain
{
    public class BlockChainComponent : IBlockChainComponent
    {
     
        private IGenesisBlockComponent genesis;
        private IBlockChainConfigComponent config;

        public List<Block> blocks { get; set; } = new List<Block>();

        public BlockChainComponent( IGenesisBlockComponent genesisComponent, IBlockChainConfigComponent config)
        {
            this.genesis = genesisComponent;
            this.config = config;
        }

        public void addBlock(Block b)
        {
            if (blocks.Any(blo => blo.blockID == b.blockID))
            {
                Console.WriteLine($"[X Block] Added block {b.blockID} already registered");
            }
            else
            {
                Console.WriteLine($"[+ Block] Added block {b.blockHash.transformToString()}");
                blocks.Add(b);
                config.saveBlock(b);
            }
     
        }

        public List<Block> GetBlocks(long maxTime = -1)
        {
            //Evaluate the second condition only if stopblock has been provided
            return blocks.Where(b => maxTime == -1 || b.createdTime < maxTime).ToList();
        }



        public void setDirectBlockChain(List<Block> blocks)
        {
            this.blocks = blocks;
            this.blocks.ForEach(t => config.saveBlock(t));
        }

        public Block getLastBlock(long maxTime = -1)
        {
            return this.GetBlocks(maxTime).OrderByDescending(b => b.createdTime).ToList().First();
        }
    }

    public interface IBlockChainComponent
    {
         List<Block> GetBlocks(long maxTime = -1);

        void addBlock(Block b);

        void setDirectBlockChain(List<Block> blocks);

        Block getLastBlock(long maxTime = -1);

    }
}
