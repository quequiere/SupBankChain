﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SupBankChain.Core.Components.BlockChain
{
    public class GenesisBlockComponent : IGenesisBlockComponent
    {
        public Block generateGenesisBlock()
        {
            Hash hash = new Hash("This is the pre genesis block");

            string quequiereWallet = "{\"Modulus\":\"ta296LXOVQHsfBUU3uD5nnJN91nFcsAKjjI5vMxVEVnwSpVF0tLTn2p8iCeIzzKZN0UZNxfzzIvRInNREUiIluI+3PuNwjbpoZotd6Avn6cJQkfprP8qCUYjSoLD2376AU6QDswrD6n6qTVQhwM1YqX+iUEnwfdqGKQVDDlK1UuNacQENlIVJgdrQZLLy2TI7RpCNqhYgZf1g1rC1Kw7s1KzbcZSJK/Wkr5ZmYfjEBDHjc57IcigxbTtoI8eBm9Z04vlZYeiKm3eBIxEGzwuJC4sMBvc21P/ibzGbGLWbvh/ECB13ikMnh1kN4h7x4oMmaujN1/dcrzmpU3EgGo1hQ==\",\"Exponent\":\"AQAB\",\"P\":null,\"Q\":null,\"DP\":null,\"DQ\":null,\"InverseQ\":null,\"D\":null}";
            string quequiereZenbookWallet = "{\"Modulus\":\"0R1IbsFpyUTB4suLiSLwai1TJ5BKkrWQ2M4xkUpuOSKH5ZqR6GaCSBOpaAt5tUDgQYWRfpVCm/lrhGrY6w+A1zOK7tE/UaIvITw9IyW5TyKRFGqs2vTRPS5Ou3rvEGCWb86Hg+cthHqycfO8m3y6ZMhvWl/NsD8VtqbdbbkxrPAETrWA2fRmyH2E91NHPg4F5IoaL8nZ0dXkfilyBdXWc34JV4eXcuzx0aKup8UBjG+Hx0XXNPahcTpZpZtHzkmkFEjo5kj+7lZknaT+QdCIECR2txln/wVg5ximcaInvjs/Jq23hf0gAd3t3lzehhnmTIKsUCq0y/+RFtOIMT5OcQ==\",\"Exponent\":\"AQAB\",\"P\":null,\"Q\":null,\"DP\":null,\"DQ\":null,\"InverseQ\":null,\"D\":null}";
            string remoteBankNode = "{\"Modulus\":\"rKWE+jPeVfWOzZ6D5kIismrx0TB+FEK+t0sEcUkq+9T37ePkpQth7L4t7+ckcZt+t1pv1pkb1+Y4J7VSg3uivGzSzp5NAw2m6EK5YV7fuj4n5iCp1FhjhVGzWYsnPSSrV5Xuf02FT9vzIVinbn/L7FEkwZowdwodWtlgpQEsXtvPtbbs7JMH+cfS1Suhg6u57wz9IerG/rvMPxEW2PMnzV6pW/ZW31jOTNxz2nf7ZbykpIOitAuet4KODmVApiRLYDspYNsTGq1e/ghwqOOXOsmUpDutbth+Qpln9thPrd9/i3xhSe0jlRGAOcIPcUTlht1mowjlqovcpKa3/rKIUQ==\",\"Exponent\":\"AQAB\",\"P\":null,\"Q\":null,\"DP\":null,\"DQ\":null,\"InverseQ\":null,\"D\":null}";





             List <TransactionOutput> originOutput = new List<TransactionOutput>
            {
                new TransactionOutput(quequiereWallet,500),
                new TransactionOutput(quequiereZenbookWallet,500),
                new TransactionOutput(remoteBankNode,Double.MaxValue-10000000000),
            };

            Transaction originTransaction = new Transaction(originOutput);
            originTransaction.setTransactionInput(new TransactionInput(1, quequiereWallet, int.MaxValue,"" ));

            Block b = new Block(hash,new List<Transaction> {originTransaction},0);

            b.setup(1547726673000, new Hash("This is the genesis block") , 2, 0,"");
            b.isGenesisBlock = true;
            return b;
        }
    }

    public interface IGenesisBlockComponent
    {
        Block generateGenesisBlock();
    }
}
