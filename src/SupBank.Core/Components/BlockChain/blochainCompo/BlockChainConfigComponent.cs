﻿using SupBankChain.Core.Components.Configs;
using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SupBankChain.Core.Components.BlockChain.blochainCompo
{
    public class BlockChainConfigComponent : IBlockChainConfigComponent
    {
        private IConfigSaverComponent saver;
        private string path = "./config/blockchainData/";

        public BlockChainConfigComponent(IConfigSaverComponent configSaverComponent)
        {
            this.saver = configSaverComponent;
        }

        public List<Block> getBlocksFromFiles()
        {
            return saver.reloadFolder<Block>(path).OrderBy(b => b.blockID).ToList();
        }

        public void clearBlocks()
        {
            saver.removeAllFile(path);
        }

        public void saveBlock(Block b)
        {
            saver.saveFile(path, $"{b.blockID}_block.json", b);
        }
    }

    public interface IBlockChainConfigComponent
    {
        void saveBlock(Block b);
        List<Block> getBlocksFromFiles();

        void clearBlocks();
    }
}
