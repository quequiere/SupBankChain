﻿using SupBankChain.Core.Model.BlockChain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SupBankChain.Core.Components.BlockChain
{
    public class BlockChainUtils
    {

        public static int minToSolve = 10;

        public static Hash hashAlgo(long createdTime, string previousHash, string data, int nonce,string publicAddress)
        {
            return new Hash($"{createdTime}{previousHash}{data}{nonce}{publicAddress}");
        }

        public static Hash hashBlock(Block block)
        {
            return hashAlgo(block.createdTime, block.previousBlockHash.transformToString(), block.getDataAsString(), block.nonce,block.minerWallet);
        }

        public static int getDifficulty(long currentTime, long lastResolveTime, int lastDifficulty)
        {
            //This represent the normal time to solve a block on the network
            int normalSolveTimeInMs = minToSolve * 60 * 1000;

            long diff = currentTime - lastResolveTime;

            int newDifficulty = diff > normalSolveTimeInMs ? lastDifficulty - 1 : lastDifficulty + 1;

            if (newDifficulty < 1)
                return 1;

            return newDifficulty;

        }
    }
}
