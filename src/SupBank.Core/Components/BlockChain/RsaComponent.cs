﻿using System;
using System.Security.Cryptography;
using System.Text;
using NETCore.Encrypt;
using NETCore.Encrypt.Internal;
using Newtonsoft.Json;
using SupBankChain.Core.Tools;

namespace SupBankChain.Core.Components.BlockChain
{
    public class RsaComponent : IRsaComponent
    {
        public string decryptMessage(string privateKey, string cryptedMessage)
        {
            return EncryptProvider.RSADecrypt(privateKey, cryptedMessage);
        }

        public string encryptMessage(string publicKey, string message)
        {
            return EncryptProvider.RSAEncrypt(publicKey, message);
        }

        public RSAKey generateKeyPair()
        {
            RSAKey rsa = EncryptProvider.CreateRsaKey();
            return rsa;
        }

        //using windows signed system cause doesn't work with NetcoreEncrypt lib (too long signed message)
        public bool verifySigned(string originalMessage, string signedMessage, string publicKey)
        {
            RSAParameters pk = JsonConvert.DeserializeObject<RSAParameters>(publicKey);

            bool success = false;
            using (var rsa = new RSACryptoServiceProvider())
            {
                var encoder = new UTF8Encoding();
                byte[] bytesToVerify = encoder.GetBytes(originalMessage);
                byte[] signedBytes = Convert.FromBase64String(signedMessage);
                try
                {
                    rsa.ImportParameters(pk);
                    SHA512Managed Hash = new SHA512Managed();
                    byte[] hashedData = Hash.ComputeHash(signedBytes);
                    success = rsa.VerifyData(bytesToVerify, CryptoConfig.MapNameToOID("SHA512"), signedBytes);
                }
                catch (CryptographicException e)
                {
                    SupLogger.log.Error(e.Message);
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return success;
        }

        //using windows signed system cause doesn't work with NetcoreEncrypt lib (too long signed message)
        public string signData(string message, string privateKey)
        {
            RSAParameters pk = JsonConvert.DeserializeObject<RSAParameters>(privateKey);
            byte[] signedBytes;
            using (var rsa = new RSACryptoServiceProvider())
            {
                var encoder = new UTF8Encoding();
                byte[] originalData = encoder.GetBytes(message);

                try
                {
                    rsa.ImportParameters(pk);
                    signedBytes = rsa.SignData(originalData, CryptoConfig.MapNameToOID("SHA512"));
                }
                catch (CryptographicException e)
                {
                    SupLogger.log.Error(e.Message);
                    return null;
                }
                finally
                {
                    rsa.PersistKeyInCsp = false;
                }
            }
            return Convert.ToBase64String(signedBytes);
        }
    }

    public interface IRsaComponent
    {
        RSAKey generateKeyPair();

        string encryptMessage(string publicKey, string message);

        string decryptMessage(string privateKey, string cryptedMessage);
        bool verifySigned(string originalMessage, string signedMessage, string publicKey);
       string signData(string message, string privateKey);

    }


}
