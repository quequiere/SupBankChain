﻿using log4net;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SupBankChain.Core.Components
{
    public class SupLogger
    {
        public static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    }
}
