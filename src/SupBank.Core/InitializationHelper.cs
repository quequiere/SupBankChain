﻿using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using log4net.Config;
using Microsoft.Extensions.DependencyInjection;
using SupBankChain.Core.Components;
using SupBankChain.Core.Components.BlockChain;
using SupBankChain.Core.Components.Network;
using SupBankChain.Core.Components.Network.p2p;
using SupBankChain.Core.Components.Network.p2p.client;
using SupBankChain.Core.Components.Network.p2p.server;
using SupBankChain.Core.Components.Network.p2p.tasks;
using SupBankChain.Core.Components.Network.Web;

//To set the internal visibility
using System.Runtime.CompilerServices;
using SupBankChain.Core.Components.BlockChain.blochainCompo;
using System;
using SupBankChain.Core.Components.Configs;
using SupBankChain.Core.Model.BlockChain;
using System.Collections.Generic;

[assembly: InternalsVisibleTo("SupBankChain.Core.Test")]

namespace SupBankChain.Core
{
    public static class InitializationHelper
    {

        public static IServiceCollection registerAllCoreSingleton(this IServiceCollection services)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());

            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            SupLogger.log.Debug("Use log4net logger");

            services.AddSingleton<INetworkComponent, NetworkComponent>();
            services.AddSingleton<IBlockChainComponent, BlockChainComponent>();
            services.AddSingleton<IGenesisBlockComponent, GenesisBlockComponent>();
            services.AddSingleton<INetworkComponent, NetworkComponent>();
            services.AddSingleton<ResfreshPeersTask>();
            services.AddSingleton<IP2PServer, P2PServer>();
            services.AddSingleton<IP2PClient, P2PClient>();
            services.AddSingleton<IStaticWebNodes, StaticWebNodes>();
            services.AddSingleton<IWebClient, WebClient>();
            services.AddSingleton<IMinerComponent, MinerComponent>();
            services.AddSingleton<IRsaComponent, RsaComponent>();

            services.AddSingleton<IWalletCompenent, WalletComponent>();
            services.AddSingleton<ITransactionComponent, TransactionComponent>();
            services.AddSingleton<ITransactionPoolComponent, TransactionPoolComponent>();
            services.AddSingleton<IProfileComponent, ProfileComponent>();
            services.AddSingleton<IConfigSaverComponent, ConfigSaverComponent>();
            services.AddSingleton<IBlockchainValidator, BlockchainValidator>();
            services.AddSingleton<IBlockChainConfigComponent, BlockChainConfigComponent>();


            services.AddSingleton<ClientMessageHandler>();
            services.AddSingleton<ServerMessageHandler>();

            return services;
        }

        public static async Task onPostInit(ServiceProvider serviceProvider,int port, bool bankmod)
        {

            IProfileComponent profile = serviceProvider.GetService<IProfileComponent>();
            profile.reloadProfile();

            if(profile.isBankAccount()!=bankmod)
                profile.setBankAccount(bankmod);

            if(profile.isBankAccount())
            {
                SupLogger.log.Info("================ BANKMOD =====================");
                SupLogger.log.Info("This instance will be running with bankmod");
                SupLogger.log.Info("This is for test only. You can loose all your token with this mod.");
                SupLogger.log.Info("================ BANKMOD =====================");
            }

            //Set static access
            SupNetwork.network = serviceProvider.GetService<INetworkComponent>();

            //start the local server and try to join peer to peer network
            await serviceProvider.GetService<INetworkComponent>().CreateLocalServer(port);
            await serviceProvider.GetService<INetworkComponent>().InitializeConnection();

            //This task must not be awaited cause will run in background
            serviceProvider.GetService<ResfreshPeersTask>().StartRefreshJob(30);

            reloadBlockChain(serviceProvider.GetService<IBlockchainValidator>(), serviceProvider.GetService<IBlockChainConfigComponent>(), serviceProvider.GetService<IBlockChainComponent>(), serviceProvider.GetService<IGenesisBlockComponent>());

        }

        public static void reloadBlockChain(IBlockchainValidator validator, IBlockChainConfigComponent blockChainSaver, IBlockChainComponent blockChain, IGenesisBlockComponent genesisCompo)
        {
            List<Block> loadedFromLocal = blockChainSaver.getBlocksFromFiles();

            if(loadedFromLocal != null && loadedFromLocal.Count>0)
            {
                SupLogger.log.Debug("Loaded blockchain data from save, try to verify blocks");
                blockChain.setDirectBlockChain(loadedFromLocal);
                if (validator.verrifyAllBlock(loadedFromLocal))
                {
                    SupLogger.log.Info($"{loadedFromLocal.Count} blocks loaded from local save !");
                    return;
                }

                SupLogger.log.Info($"Local blockchain seems to be corrupted and will no be loaded.");
                blockChainSaver.clearBlocks();
            }

            List<Block> finalResult = new List<Block>();
            Block genesis = genesisCompo.generateGenesisBlock();
            finalResult.Add(genesis);
            blockChainSaver.saveBlock(genesis);

            blockChain.setDirectBlockChain(finalResult);

            SupLogger.log.Info($"New local block chain has been initialized");
        }




    }
}
