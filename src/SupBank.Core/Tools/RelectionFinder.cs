﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SupBankChain.Core.Model.Network.Messaging;
// ReSharper disable InconsistentlySynchronizedField

namespace SupBankChain.Core.Tools
{
    public class RelectionFinder
    {
        private static readonly List<Type> PossibleMessageTypes = new List<Type>();

        //Use reflection to list all message type possible. Save the result in cache cause reflection use lot of resources
        private static List<Type> GetAllP2PMessageDefinition()
        {
            if (PossibleMessageTypes.Count>0)
                return PossibleMessageTypes;

            //We lock cause sometimes two messages or more can call this method in same time. And when possibleMessage
            //is not initialized, we can have some strange result.

            lock (PossibleMessageTypes)
            {

                foreach (Type type in Assembly.GetAssembly(typeof(P2PMessage)).GetTypes().Where(type =>
                    type.IsClass && !type.IsAbstract && type.IsSubclassOf(typeof(P2PMessage))))
                {
                    PossibleMessageTypes.Add(type);
                }
            }

            return PossibleMessageTypes;
        }

        public static Type getP2ptypeByName(String typeName)
        {
            try
            {
                return GetAllP2PMessageDefinition().FirstOrDefault(type => type.Name.Equals(typeName));
            }
            catch (Exception e)
            {
                return null;
            }
           
        }

    }
}
